<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>central-pom</artifactId>
        <version>6.3.7</version>
    </parent>

    <groupId>com.atlassian.connect</groupId>
    <artifactId>atlassian-connect-spring-boot</artifactId>
    <version>5.1.4-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>Atlassian Connect Spring Boot</name>
    <description>Provides support for Atlassian Connect add-ons using Spring Boot</description>
    <url>https://bitbucket.org/atlassian/atlassian-connect-spring-boot</url>

    <developers>
        <developer>
            <organization>Atlassian</organization>
        </developer>
    </developers>

    <modules>
        <module>atlassian-connect-spring-boot-api</module>
        <module>atlassian-connect-spring-boot-core</module>
        <module>atlassian-connect-spring-boot-jwt</module>
        <module>atlassian-connect-spring-boot-starter</module>
        <module>atlassian-connect-spring-boot-jpa-starter</module>
        <module>atlassian-connect-spring-boot-archetype</module>
    </modules>

    <scm>
        <url>https://bitbucket.org/atlassian/atlassian-connect-spring-boot</url>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/atlassian-connect-spring-boot.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/atlassian-connect-spring-boot.git</developerConnection>
        <tag>HEAD</tag>
    </scm>
    <issueManagement>
        <system>JIRA</system>
        <url>https://ecosystem.atlassian.net/browse/ACSPRING</url>
    </issueManagement>

    <properties>
        <!-- Java version -->
        <java.version>17</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>

        <!-- Spring Boot version, update should be reflected in updating Javadoc link versions below -->
        <spring-boot.version>3.2.7</spring-boot.version>

        <!-- Build and test versions -->
        <jacoco.version>0.8.12</jacoco.version>
        <maven.archetype.version>3.2.1</maven.archetype.version>
        <maven.javadoc.plugin.version>3.5.0</maven.javadoc.plugin.version>
        <tidy.version>1.2.0</tidy.version>

        <!-- Javadoc link versions (manually versioned) -->
        <spring-framework.version>6.1.10</spring-framework.version>
        <spring-data-commons.version>3.2.7</spring-data-commons.version>
        <spring-security.version>6.2.5</spring-security.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco.version}</version>
                    <executions>
                        <execution>
                            <id>prepare-agent</id>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>tidy-maven-plugin</artifactId>
                <version>${tidy.version}</version>
                <executions>
                    <execution>
                        <id>check</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>dependency-convergence</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <dependencyConvergence />
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>it</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <modules>
                <module>atlassian-connect-spring-boot-it</module>
            </modules>
        </profile>
    </profiles>
</project>
