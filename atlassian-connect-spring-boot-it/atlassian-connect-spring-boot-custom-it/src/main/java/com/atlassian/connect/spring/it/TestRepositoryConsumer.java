package com.atlassian.connect.spring.it;

import org.springframework.stereotype.Component;

@Component
public class TestRepositoryConsumer {

    private final TestRepository testRepository;

    public TestRepositoryConsumer(TestRepository testRepository) {
        this.testRepository = testRepository;
    }
}
