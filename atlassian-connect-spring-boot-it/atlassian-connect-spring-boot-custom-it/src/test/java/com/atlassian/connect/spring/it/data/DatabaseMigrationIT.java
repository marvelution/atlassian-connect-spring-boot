package com.atlassian.connect.spring.it.data;

import com.atlassian.connect.spring.it.TestRepository;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class DatabaseMigrationIT extends BaseApplicationIT {

    @Autowired
    private TestRepository testRepository;

    @Test
    void canUseRepositoryAfterLiquibaseMigration() {
        assertThat(testRepository.count(), is(0L));
    }
}
