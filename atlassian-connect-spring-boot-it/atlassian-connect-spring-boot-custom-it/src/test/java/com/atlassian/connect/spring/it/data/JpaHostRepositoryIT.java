package com.atlassian.connect.spring.it.data;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(properties = {
        "spring.jpa.hibernate.ddl-auto=update",
        "spring.liquibase.enabled=false"
})
class JpaHostRepositoryIT {

    @Autowired
    private AtlassianHostRepository hostRepository;

    @AfterEach
    public void tearDown() {
        hostRepository.deleteAll();
    }

    @Test
    void shouldHostRepositoryBeEmpty() {
        assertThat(hostRepository.count(), is(0L));
    }

    @Test
    void shouldHostRepositoryHaveOneHostAfterSavingOne() {
        createAndSaveHost(hostRepository);
        assertThat(hostRepository.count(), is(1L));
    }

    @Test
    void shouldFindOneOfMultipleSavedHostsByBaseUrlOrderByLastModifiedDateDesc() {
        createAndSaveHost(hostRepository);
        AtlassianHost otherHost = new AtlassianHostBuilder().clientKey("some-other-host").build();
        hostRepository.save(otherHost);
        Optional<AtlassianHost> firstByBaseUrl = hostRepository.findFirstByBaseUrlOrderByLastModifiedDateDesc(otherHost.getBaseUrl());
        assertThat(firstByBaseUrl, equalTo(of(otherHost)));
    }

    @Test
    void shouldFindSavedHostByClientKey() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHost foundHost = hostRepository.findById(host.getClientKey()).get();
        assertThat(foundHost, equalTo(host));
    }
}
