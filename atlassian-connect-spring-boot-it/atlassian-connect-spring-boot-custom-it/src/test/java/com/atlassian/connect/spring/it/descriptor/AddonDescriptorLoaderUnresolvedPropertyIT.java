package com.atlassian.connect.spring.it.descriptor;

import ch.qos.logback.classic.Level;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.LogbackCaptureExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.atlassian.connect.spring.it.util.LogbackEventMatcher.logbackEvent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;


@SpringBootTest
class AddonDescriptorLoaderUnresolvedPropertyIT extends BaseApplicationIT {

    @RegisterExtension
    public final LogbackCaptureExtension logbackCaptureExtension = new LogbackCaptureExtension();

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    void shouldWarnOnUnresolvedPlaceholder() {
        addonDescriptorLoader.getRawDescriptor();
        String message = "Add-on descriptor contains unresolved configuration placeholder: Could not resolve placeholder 'add-on.undefined-port'";
        assertThat(logbackCaptureExtension.getEvents(), hasItem(logbackEvent(is(Level.WARN), containsString(message))));
    }
}
