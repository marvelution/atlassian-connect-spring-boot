package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

@SpringBootTest
class AddonDescriptorLoaderPropertyResolutionIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    void shouldResolveAddonKeyPlaceholder() {
        AddonDescriptor descriptor = addonDescriptorLoader.getDescriptor();
        assertThat(descriptor.getKey(), is("atlassian-connect-spring-boot-custom-it"));
    }

    @Test
    void shouldResolveLifecycleUrlPlaceholders() {
        assertThat(addonDescriptorLoader.getDescriptor().getInstalledLifecycleUrl(), is("/custom-installed"));
        assertThat(addonDescriptorLoader.getDescriptor().getEnabledLifecycleUrl(), nullValue());
        assertThat(addonDescriptorLoader.getDescriptor().getDisabledLifecycleUrl(), nullValue());
        assertThat(addonDescriptorLoader.getDescriptor().getUninstalledLifecycleUrl(), is("/custom-uninstalled"));
    }
}
