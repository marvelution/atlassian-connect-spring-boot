package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class AddonDescriptorControllerPropertyResolutionIT extends BaseApplicationIT {

    @Value("${add-on.key}")
    private String addonKey;

    @Test
    void shouldReplacePropertyPlaceholdersInDescriptor() throws Exception {
        mvc.perform(get("/atlassian-connect.json")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.key").value(is(addonKey)));
    }
}
