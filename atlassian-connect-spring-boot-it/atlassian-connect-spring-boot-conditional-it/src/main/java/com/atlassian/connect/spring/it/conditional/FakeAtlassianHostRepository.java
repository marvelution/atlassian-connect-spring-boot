package com.atlassian.connect.spring.it.conditional;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import jakarta.annotation.Nonnull;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class FakeAtlassianHostRepository implements AtlassianHostRepository {

    @Override
    public Optional<AtlassianHost> findFirstByBaseUrlOrderByLastModifiedDateDesc(String baseUrl) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Nonnull
    public <S extends AtlassianHost> S save(@Nonnull S entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Nonnull
    public <S extends AtlassianHost> Iterable<S> saveAll(@Nonnull Iterable<S> entities) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Nonnull
    public Optional<AtlassianHost> findById(@Nonnull String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean existsById(@Nonnull String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Nonnull
    public Iterable<AtlassianHost> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    @Nonnull
    public Iterable<AtlassianHost> findAllById(@Nonnull Iterable<String> strings) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long count() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(@Nonnull String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(@Nonnull AtlassianHost entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAllById(@Nonnull Iterable<? extends String> strings) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAll(@Nonnull Iterable<? extends AtlassianHost> entities) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAll() {
        throw new UnsupportedOperationException();
    }
}
