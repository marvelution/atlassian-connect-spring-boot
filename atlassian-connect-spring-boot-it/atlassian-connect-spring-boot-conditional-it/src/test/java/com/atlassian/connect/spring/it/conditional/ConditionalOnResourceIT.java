package com.atlassian.connect.spring.it.conditional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyIterable;

@SpringBootTest
@DirtiesContext
class ConditionalOnResourceIT {

    @Autowired
    private WebApplicationContext wac;

    @Test
    void shouldNotRegisterAnyBeansWithoutAddonDescriptorPresent() {
        assertThat(getStarterBeanClasses(), is(emptyIterable()));
    }

    private List<? extends Class<?>> getStarterBeanClasses() {
        return Arrays.stream(wac.getBeanDefinitionNames())
                .map(wac::getType)
                .filter(Objects::nonNull)
                .filter(this::isStarterClass)
                .collect(Collectors.toList());
    }

    private boolean isStarterClass(Class<?> c) {
        String packageName = c.getPackage().getName();
        return packageName.startsWith("com.atlassian.connect.spring")
                && !packageName.startsWith("com.atlassian.connect.spring.it.conditional");
    }
}
