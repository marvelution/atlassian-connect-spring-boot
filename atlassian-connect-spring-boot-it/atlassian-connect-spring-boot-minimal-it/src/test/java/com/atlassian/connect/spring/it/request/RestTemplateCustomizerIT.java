package com.atlassian.connect.spring.it.request;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.request.oauth2.OAuth2JwtAccessTokenResponseClient;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import jakarta.annotation.Nonnull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.client.RestTemplate;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class RestTemplateCustomizerIT extends BaseApplicationIT {

    private static final DummyClientHttpRequestInterceptor DUMMY_REQUEST_INTERCEPTOR = new DummyClientHttpRequestInterceptor();

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Autowired
    private OAuth2JwtAccessTokenResponseClient oAuth2JwtAccessTokenResponseClient;

    private AtlassianHost host;

    @BeforeEach
    public void setUp() {
        host = createAndSaveHost(hostRepository);
    }

    @Test
    void shouldCustomizeJwtRestTemplate() {
        assertRestTemplateHasBadRequestInterceptor(atlassianHostRestClients.authenticatedAsAddon());
    }

    @Test
    @WithMockUser
    void shouldCustomizeOAuth2RestTemplate() {
        AtlassianHostUser hostUser = AtlassianHostUser.builder(host)
                .withUserAccountId("cab9a26e-56ec-49e9-a08f-d7e4a19bde55").build();
        assertRestTemplateHasBadRequestInterceptor(atlassianHostRestClients.authenticatedAs(hostUser));
    }

    @Test
    void shouldCustomizeAccessTokenProviderRestTemplate() {
        assertRestTemplateHasBadRequestInterceptor(oAuth2JwtAccessTokenResponseClient.getRestTemplate());
    }

    private void assertRestTemplateHasBadRequestInterceptor(RestTemplate restTemplate) {
        assertThat(restTemplate.getInterceptors(), hasItem(DUMMY_REQUEST_INTERCEPTOR));
    }

    @TestConfiguration
    public static class RestTemplateCustomizerConfiguration {

        @Bean
        public RestTemplateCustomizer badInterceptorCustomizer() {
            return restTemplate -> restTemplate.getInterceptors().add(DUMMY_REQUEST_INTERCEPTOR);
        }
    }

    private static class DummyClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

        @Override
        @Nonnull
        public ClientHttpResponse intercept(@Nonnull HttpRequest request,
                                            @Nonnull byte[] body,
                                            @Nonnull ClientHttpRequestExecution execution) {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
