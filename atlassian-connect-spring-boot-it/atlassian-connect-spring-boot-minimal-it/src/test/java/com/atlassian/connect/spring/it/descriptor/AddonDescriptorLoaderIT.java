package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;

@SpringBootTest
class AddonDescriptorLoaderIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    void shouldReturnRawDescriptor() {
        String rawDescriptor = addonDescriptorLoader.getRawDescriptor();
        assertThat(rawDescriptor, startsWith("{"));
    }

    @Test
    void shouldReturnAddonKey() {
        AddonDescriptor descriptor = addonDescriptorLoader.getDescriptor();
        assertThat(descriptor.getKey(), notNullValue());
    }

    @Test
    void shouldReturnBaseUrl() {
        assertThat(addonDescriptorLoader.getDescriptor().getBaseUrl(), notNullValue());
    }

    @Test
    void shouldReturnAuthenticationType() {
        assertThat(addonDescriptorLoader.getDescriptor().getAuthenticationType(), notNullValue());
    }

    @Test
    void shouldReturnLifecycleUrls() {
        assertThat(addonDescriptorLoader.getDescriptor().getInstalledLifecycleUrl(), notNullValue());
        assertThat(addonDescriptorLoader.getDescriptor().getUninstalledLifecycleUrl(), notNullValue());
    }
}
