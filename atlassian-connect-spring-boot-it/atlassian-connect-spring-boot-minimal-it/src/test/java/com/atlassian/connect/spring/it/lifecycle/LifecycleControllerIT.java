package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostMapping;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.auth.jwt.InvalidAsymmetricJwtException;
import com.atlassian.connect.spring.internal.lifecycle.LifecycleAuthenticator;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.BASE_URL;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.INSTALLATION_ID;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.OTHER_SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.testSecurityContext;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class LifecycleControllerIT extends BaseLifecycleControllerIT {
    private static final String USER_ACCOUNT_ID = "123:ACCID";
    private static final String ENTITLEMENT_ID = "entitlement-id";
    private static final String ENTITLEMENT_NUMBER = "entitlement-number";

    @Autowired
    AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    @Autowired
    AtlassianConnectProperties properties;

    @MockBean
    LifecycleAuthenticator lifecycleAuthenticator;

    @Test
    void shouldStoreHostOnFirstInstall() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);

        mvc.perform(postInstalled("/installed", SHARED_SECRET).with(testSecurityContext()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.getDisplayUrl(), is(BASE_URL));
        assertThat(installedHost.getDisplayUrlServicedeskHelpCenter(), is(BASE_URL));
        assertThat(installedHost.isAddonInstalled(), is(true));

        assertThat(installedHost.getCreatedDate(), is(notNullValue()));
        assertThat(installedHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(installedHost.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(installedHost.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));

        assertThat(installedHost.getEntitlementNumber(), equalTo(ENTITLEMENT_NUMBER));
        assertThat(installedHost.getEntitlementId(), equalTo(ENTITLEMENT_ID));

        assertNull(installedHost.getCapabilitySet());
    }

    @Test
    void shouldStoreHostOnFirstInstallWithCapabilitySet() throws Exception {
        String capabilitySet = "capabilityStandard";
        AtlassianHost host = new AtlassianHostBuilder()
                .clientKey(CLIENT_KEY)
                .capabilitySet(capabilitySet)
                .build();

        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);

        mvc.perform(postInstalledWithCapabilitySet("/installed", capabilitySet).with(testSecurityContext()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.getDisplayUrl(), is(BASE_URL));
        assertThat(installedHost.getDisplayUrlServicedeskHelpCenter(), is(BASE_URL));
        assertThat(installedHost.isAddonInstalled(), is(true));

        assertThat(installedHost.getCreatedDate(), is(notNullValue()));
        assertThat(installedHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(installedHost.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(installedHost.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));

        assertThat(installedHost.getEntitlementNumber(), equalTo(ENTITLEMENT_NUMBER));
        assertThat(installedHost.getEntitlementId(), equalTo(ENTITLEMENT_ID));

        assertThat(installedHost.getCapabilitySet(), equalTo(capabilitySet));
    }

    @Test
    void shouldStoreHostAndMappingOnHarmonizedInstall() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).buildWithInstallationId();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);

        mvc.perform(harmonizedPostInstalled("/installed", INSTALLATION_ID).with(testSecurityContext()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getInstallationId(), is(INSTALLATION_ID));

        AtlassianHostMapping savedMapping = hostMappingRepository.findById(INSTALLATION_ID).get();
        assertThat(savedMapping, notNullValue());
        assertThat(savedMapping.getClientKey(), is(CLIENT_KEY));
        assertThat(savedMapping.getInstallationId(), is(INSTALLATION_ID));
    }

    @Test
    void shouldRemoveOldMappingOnClassicInstallAfterHarmonizedInstall() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).buildWithInstallationId();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);

        mvc.perform(harmonizedPostInstalled("/installed", INSTALLATION_ID).with(testSecurityContext()))
                .andExpect(status().isNoContent());

        AtlassianHost firstHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(firstHost, notNullValue());
        assertThat(firstHost.getInstallationId(), is(INSTALLATION_ID));

        AtlassianHostMapping firstMapping = hostMappingRepository.findById(INSTALLATION_ID).get();
        assertThat(firstMapping, notNullValue());
        assertThat(firstMapping.getClientKey(), is(CLIENT_KEY));
        assertThat(firstMapping.getInstallationId(), is(INSTALLATION_ID));

        mvc.perform(postInstalled("/installed", SHARED_SECRET).with(testSecurityContext()))
                .andExpect(status().isNoContent());

        AtlassianHost secondHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(secondHost, notNullValue());
        assertThat(secondHost.getInstallationId(), nullValue());

        Optional<AtlassianHostMapping> optionalMapping = hostMappingRepository.findById(INSTALLATION_ID);
        assertThat(optionalMapping.isPresent(), is(false));
    }

    @Test
    void shouldAllowMultipleInstallationIdsForSameClientKey() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).buildWithInstallationId();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        List<String> installationIds = Arrays.asList(
                "ari-installation-id-1",
                "ari-installation-id-2",
                "ari-installation-id-3",
                "ari-installation-id-4",
                "ari-installation-id-5"
        );

        for (String installationId : installationIds) {
            mvc.perform(harmonizedPostInstalled("/installed", installationId).with(testSecurityContext()))
                    .andExpect(status().isNoContent());
        }

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        // The associated installation_id should be the most recent one
        assertThat(installedHost.getInstallationId(), equalTo(installationIds.get(installationIds.size() - 1)));

        for (String installationId : installationIds) {
            AtlassianHostMapping savedMapping = hostMappingRepository.findById(installationId).get();
            assertThat(savedMapping, notNullValue());
            assertThat(savedMapping.getClientKey(), is(CLIENT_KEY));
            assertThat(savedMapping.getInstallationId(), equalTo(installationId));
        }
    }

    @Test
    void shouldDeleteMappingOnHostDeletion() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).buildWithInstallationId();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);

        mvc.perform(harmonizedPostInstalled("/installed", INSTALLATION_ID).with(testSecurityContext()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getInstallationId(), is(INSTALLATION_ID));

        hostRepository.deleteById(CLIENT_KEY);

        Optional<AtlassianHostMapping> optionalMapping = hostMappingRepository.findById(INSTALLATION_ID);
        assertThat(optionalMapping.isPresent(), is(false));
    }

    @Test
    void shouldRetainHostOnMappingDeletion() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).buildWithInstallationId();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);

        mvc.perform(harmonizedPostInstalled("/installed", INSTALLATION_ID).with(testSecurityContext()))
                .andExpect(status().isNoContent());

        AtlassianHostMapping savedMapping = hostMappingRepository.findById(INSTALLATION_ID).get();
        assertThat(savedMapping, notNullValue());
        assertThat(savedMapping.getClientKey(), is(CLIENT_KEY));

        hostMappingRepository.deleteById(INSTALLATION_ID);

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getInstallationId(), is(INSTALLATION_ID));
    }


    @Test
    void shouldUpdateSharedSecretOnSignedSecondInstall() throws Exception {
        AtlassianHost host = saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        Calendar creationDate = host.getCreatedDate();
        Calendar creationLastModified = host.getLastModifiedDate();

        String newSharedSecret = "some-other-secret";
        mvc.perform(postInstalled("/installed", newSharedSecret))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(newSharedSecret));

        assertThat(installedHost.getCreatedDate(), is(notNullValue()));
        assertThat(installedHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(installedHost.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(installedHost.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));

        assertThat(installedHost.getEntitlementNumber(), equalTo(ENTITLEMENT_NUMBER));
        assertThat(installedHost.getEntitlementId(), equalTo(ENTITLEMENT_ID));

        assertEquals("Creation date should not change", creationDate, installedHost.getCreatedDate());
        assertNotEquals("Last Modified date should be updated", creationLastModified, installedHost.getLastModifiedDate());
    }

    @Test
    void shouldRejectSecondInstallWithoutJwt() throws Exception {
        doThrow(new InvalidAsymmetricJwtException("")).when(lifecycleAuthenticator).setAuthentication(any(HttpServletRequest.class), any(String.class));
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        mvc.perform(postInstalled("/installed", SHARED_SECRET))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void shouldRejectSharedSecretUpdateByOtherHost() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        AtlassianHost otherHost = saveHost("other-host", OTHER_SHARED_SECRET, "http://other-example.com");
        mockAsymmetricAuthentication(lifecycleAuthenticator, otherHost);
        mvc.perform(postInstalled("/installed", "some-other-secret"))
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldRejectUninstallByOtherHost() throws Exception {
        saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        AtlassianHost otherHost = saveHost("other-host", OTHER_SHARED_SECRET, "http://other-example.com");
        mockAsymmetricAuthentication(lifecycleAuthenticator, otherHost);
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldSoftDeleteHostOnUninstall() throws Exception {
        AtlassianHost host = saveHost(CLIENT_KEY, SHARED_SECRET, BASE_URL);
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.getDisplayUrl(), is(BASE_URL));
        assertThat(installedHost.getDisplayUrlServicedeskHelpCenter(), is(BASE_URL));
        assertThat(installedHost.getCapabilitySet(), is("capabilityStandard"));
        assertThat(installedHost.isAddonInstalled(), is(false));

        assertThat(installedHost.getCreatedDate(), is(notNullValue()));
        assertThat(installedHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(installedHost.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(installedHost.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));
    }

    @Test
    void shouldIgnoreMissingHostOnUninstall() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(postUninstalled("/uninstalled", SHARED_SECRET))
                .andExpect(status().isNoContent());

        Optional<AtlassianHost> installedHost = hostRepository.findById(CLIENT_KEY);
        assertThat(installedHost, is(Optional.empty()));
    }

    @Test
    void shouldRejectInstallWithoutBody() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(post("/installed")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldRejectInstallWithInvalidBody() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(post("/installed")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldRejectInstallWithInvalidEventType() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(post("/installed")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createLifecycleJson("uninstalled", "some-secret")))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldRejectUninstallWithoutBody() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(post("/uninstalled")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldRejectUninstallWithInvalidBody() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(post("/uninstalled")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldRejectUninstallWithInvalidEventType() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host, USER_ACCOUNT_ID);
        mvc.perform(post("/uninstalled")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createLifecycleJson("installed", "some-secret")))
                .andExpect(status().isBadRequest());
    }

    private AtlassianHost saveHost(String clientKey, String sharedSecret, String baseUrl) {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(clientKey);
        host.setSharedSecret(sharedSecret);
        host.setBaseUrl(baseUrl);
        host.setDisplayUrl(baseUrl);
        host.setDisplayUrlServicedeskHelpCenter(baseUrl);
        host.setCapabilitySet("capabilityStandard");
        host.setCreatedBy(USER_ACCOUNT_ID);
        host.setLastModifiedBy(USER_ACCOUNT_ID);
        host.setCreatedDate(new GregorianCalendar());
        host.setLastModifiedDate(new GregorianCalendar());
        host = hostRepository.save(host);

        assertThat(host.getCreatedDate(), is(notNullValue()));
        assertThat(host.getLastModifiedDate(), is(notNullValue()));
        assertThat(host.getCreatedBy(), equalTo(USER_ACCOUNT_ID));
        assertThat(host.getLastModifiedBy(), equalTo(USER_ACCOUNT_ID));
        return host;
    }
}
