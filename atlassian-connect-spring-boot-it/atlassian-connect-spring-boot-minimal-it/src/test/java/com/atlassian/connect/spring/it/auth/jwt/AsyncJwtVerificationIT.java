package com.atlassian.connect.spring.it.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.task.DelegatingSecurityContextAsyncTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.concurrent.Callable;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AsyncJwtVerificationIT extends BaseApplicationIT {

    @Test
    void shouldReturnPrincipalDetailsForAsyncRequest() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "cab9a26e-56ec-49e9-a08f-d7e4a19bde55";
        TestRestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, Optional.of(subject));
        ResponseEntity<AtlassianHostUser> response = restTemplate.getForEntity(getPrincipalRequestUri(), AtlassianHostUser.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserAccountId(), is(Optional.of(subject)));
    }

    private URI getPrincipalRequestUri() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress())).path("/jwt").build().toUri();
    }

    @TestConfiguration
    public static class AsyncJwtVerificationConfiguration {

        @Bean
        public AsyncJwtPrincipalController jwtPrincipalController() {
            return new AsyncJwtPrincipalController();
        }
    }

    @TestConfiguration
    public static class JwtAsyncWebMvcConfigurerAdapter implements WebMvcConfigurer {

        @Override
        public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
            configurer.setTaskExecutor(new DelegatingSecurityContextAsyncTaskExecutor(new SimpleAsyncTaskExecutor()));
        }
    }

    @RestController
    public static class AsyncJwtPrincipalController {

        @GetMapping(value = "/jwt", produces = "application/json")
        public Callable<AtlassianHostUser> getPrincipal() {
            return () -> new AtlassianConnectSecurityContextHelper().getHostUserFromSecurityContext().get();
        }
    }
}
