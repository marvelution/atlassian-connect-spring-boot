package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AddonInstalledEvent;
import com.atlassian.connect.spring.AddonUninstalledEvent;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.lifecycle.LifecycleAuthenticator;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.LifecycleBodyHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;

import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TransferQueue;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class LifecycleControllerEventIT extends BaseLifecycleControllerIT {

    @MockBean
    private LifecycleAuthenticator lifecycleAuthenticator;

    @Autowired
    private LifecycleEventCollector addonLifecycleEventCollector;

    @BeforeEach
    public void setUp() {
        addonLifecycleEventCollector.clearEvents();
    }

    @Test
    void shouldFireEventOnInstall() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host);
        mvc.perform(post("/installed")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(LifecycleBodyHelper.createLifecycleJson("installed", SHARED_SECRET)))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        AddonInstalledEvent installedEvent = addonLifecycleEventCollector.takeInstalledEvent();
        assertThat(installedEvent.getHost(), equalTo(installedHost));
    }

    @Test
    void shouldFireEventOnUninstall() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).sharedSecret(SHARED_SECRET)
                .baseUrl(AtlassianHosts.BASE_URL).build();
        hostRepository.save(host);
        mockAsymmetricAuthentication(lifecycleAuthenticator, host);
        mvc.perform(post("/uninstalled")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(LifecycleBodyHelper.createLifecycleJson("uninstalled", SHARED_SECRET)))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        AddonUninstalledEvent uninstalledEvent = addonLifecycleEventCollector.takeUninstalledEvent();
        assertThat(uninstalledEvent.getHost(), equalTo(installedHost));
    }

    @TestConfiguration
    public static class EventCollectorConfiguration {

        @Bean
        public LifecycleEventCollector lifecycleEventCollector() {
            return new LifecycleEventCollector();
        }
    }

    public static class LifecycleEventCollector {

        private static final int POLL_TIMEOUT_SECONDS = 5;

        private final TransferQueue<AddonInstalledEvent> installedEvents = new LinkedTransferQueue<>();

        private final TransferQueue<AddonUninstalledEvent> uninstalledEvents = new LinkedTransferQueue<>();

        @EventListener
        public void handleAddonInstalledEvent(AddonInstalledEvent installedEvent) {
            installedEvents.add(installedEvent);

            // An exception thrown here will not fail the request to the /installed resource
            throw new LifecycleEventException();
        }

        @EventListener
        public void handleAddonUninstalledEvent(AddonUninstalledEvent uninstalledEvent) {
            uninstalledEvents.add(uninstalledEvent);

            // An exception thrown here will not fail the request to the /uninstalled resource
            throw new LifecycleEventException();
        }

        public AddonInstalledEvent takeInstalledEvent() throws InterruptedException {
            AddonInstalledEvent event = installedEvents.poll(POLL_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            assert installedEvents.isEmpty();
            return event;
        }

        public AddonUninstalledEvent takeUninstalledEvent() throws InterruptedException {
            AddonUninstalledEvent event = uninstalledEvents.poll(POLL_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            assert uninstalledEvents.isEmpty();
            return event;
        }

        public void clearEvents() {
            installedEvents.clear();
            uninstalledEvents.clear();
        }
    }

    private static class LifecycleEventException extends RuntimeException {}
}
