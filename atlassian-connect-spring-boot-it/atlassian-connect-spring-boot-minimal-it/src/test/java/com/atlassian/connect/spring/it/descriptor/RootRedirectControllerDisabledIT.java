package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "atlassian.connect.redirect-root-to-descriptor=false")
class RootRedirectControllerDisabledIT extends BaseApplicationIT {

    @Test
    void shouldNotRedirectRootToDescriptor() throws Exception {
        mvc.perform(get("/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
