package com.atlassian.connect.spring.it.request.jwt;

import com.atlassian.connect.spring.AddonAuthenticationType;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.test.web.client.ResponseActions;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHostWithAuthenticationAndCloudId;
import static com.atlassian.connect.spring.it.util.ConnectRequestMatchers.noAuthorizationHeader;
import static com.atlassian.connect.spring.it.util.ConnectRequestMatchers.onlyOneAuthorizationHeader;
import static com.atlassian.connect.spring.it.util.ConnectRequestMatchers.userAgentHeader;import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest
class JwtSigningIT extends BaseApplicationIT {

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Value("${atlassian.connect.client-version}")
    private String atlassianConnectClientVersion;

    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        restTemplate = atlassianHostRestClients.authenticatedAsAddon();
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    public void setUp(AtlassianHost host) {
        restTemplate = atlassianHostRestClients.authenticatedAsAddon(host);
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    void shouldNotSignRequestToUnknownHostWithoutAuthenticatedHost() {
        expectAndMakeRequestWithoutJwt("http://some-other-host.com/");
    }

    @Test
    void shouldNotSignRelativeRequestWithoutAuthenticatedHost() {
        expectAndMakeRequestWithoutJwt(getRelativeRequestUrl());
    }

    @Test
    void shouldSignRequestToStoredHostWithoutAuthenticatedHost() {
        createAndSaveHost(hostRepository);
        String url = getAbsoluteRequestUrl();
        expectAndMakeRequestWithJwt(url, url);
    }

    @Test
    void shouldSignAbsoluteRequestToAuthenticatedHost() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setJwtAuthenticatedPrincipal(host);
        String url = getAbsoluteRequestUrl();
        expectAndMakeRequestWithJwt(url, url);
    }

    @Test
    void shouldSignAbsoluteRequestToFixedHost() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setUp(host);
        setJwtAuthenticatedPrincipal(host);
        String url = getAbsoluteRequestUrl();
        expectAndMakeRequestWithJwt(url, url);
    }

    @Test
    void shouldRejectAbsoluteRequestToOtherThanFixedHost() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setUp(host);
        setJwtAuthenticatedPrincipal(host);
        String url = "http://other-host.com/product" + getRelativeRequestUrl();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> expectAndMakeRequestWithJwt(url, url));
        assertThat(exception.getMessage(), is("The given URI is not under the base URL of the given host"));
    }

    @Test
    void shouldSignRelativeRequestToAuthenticatedHost() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setJwtAuthenticatedPrincipal(host);
        String relativeUrl = getRelativeRequestUrl();
        expectAndMakeRequestWithJwt(relativeUrl, AtlassianHosts.BASE_URL + relativeUrl);
    }

    @Test
    void shouldSignRelativeRequestToAuthenticatedHostAfterEnrolledOAuth2ButUseJWTAuth() {
        AtlassianHost host = createAndSaveHostWithAuthenticationAndCloudId(hostRepository, AddonAuthenticationType.JWT);
        setJwtAuthenticatedPrincipal(host);
        String relativeUrl = getRelativeRequestUrl();
        expectAndMakeRequestWithJwt(relativeUrl, AtlassianHosts.BASE_URL + relativeUrl);
    }

    @Test
    void shouldSignRelativeRequestToFixedHost() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setUp(host);
        setJwtAuthenticatedPrincipal(host);
        String relativeUrl = getRelativeRequestUrl();
        expectAndMakeRequestWithJwt(relativeUrl, AtlassianHosts.BASE_URL + relativeUrl);
    }

    @Test
    void shouldNotSignAbsoluteRequestToOtherThanAuthenticatedHost() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setJwtAuthenticatedPrincipal(host);
        expectAndMakeRequestWithoutJwt("http://other-host.com");
    }

    @Test
    void shouldNotAddMultipleJwtHeadersToRequest() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setJwtAuthenticatedPrincipal(host);
        String url = getAbsoluteRequestUrl();

        withACustomInterceptor(() -> {
            expectRequestWithJwt(url, onlyOneAuthorizationHeader());
            restTemplate.getForObject(url, Void.class);
            mockServer.verify();
        });
    }

    @Test
    void shouldOverrideRequestAuthorizationHeader() {
        createAndSaveHost(hostRepository);

        String requestUrl = getAbsoluteRequestUrl();
        expectRequestWithJwt(requestUrl, onlyOneAuthorizationHeader());

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Some value");
        HttpEntity<Void> httpEntity = new HttpEntity<>(null, headers);
        restTemplate.exchange(requestUrl, HttpMethod.GET, httpEntity, Void.class);
        mockServer.verify();
    }

    private void expectAndMakeRequestWithoutJwt(String requestUrl) {
        mockServer.expect(requestTo(requestUrl))
                .andExpect(noAuthorizationHeader())
                .andRespond(withSuccess());
        restTemplate.getForObject(requestUrl, Void.class);
        mockServer.verify();
    }

    private void expectAndMakeRequestWithJwt(String requestUrl, String expectedRequestUrl) {
        expectRequestWithJwt(expectedRequestUrl);
        restTemplate.getForObject(requestUrl, Void.class);
        mockServer.verify();
    }

    private void expectRequestWithJwt(String url, RequestMatcher... requestMatchers) {
        String addonKey = addonDescriptorLoader.getDescriptor().getKey();
        ResponseActions responseActions = mockServer.expect(requestTo(url))
                .andExpect(method(HttpMethod.GET))
                .andExpect(authorizationHeaderWithJwt())
                .andExpect(userAgentHeader(atlassianConnectClientVersion, addonKey));
        for (RequestMatcher requestMatcher : requestMatchers) {
            responseActions.andExpect(requestMatcher);
        }
        responseActions.andRespond(withSuccess());
    }

    private String getAbsoluteRequestUrl() {
        return AtlassianHosts.BASE_URL + getRelativeRequestUrl();
    }

    private String getRelativeRequestUrl() {
        return "/api";
    }

    private RequestMatcher authorizationHeaderWithJwt() {
        return header(HttpHeaders.AUTHORIZATION, startsWith("JWT "));
    }

    @SuppressWarnings("unused")
    private void withACustomInterceptor(Runnable runnable) {
        List<ClientHttpRequestInterceptor> originalInterceptors = restTemplate.getInterceptors();
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>(restTemplate.getInterceptors());

        ClientHttpRequestInterceptor innocentInterceptor = (request, body, execution) -> {
            HttpHeaders unused = request.getHeaders();
            return execution.execute(request, body);
        };
        interceptors.add(innocentInterceptor);

        restTemplate.setInterceptors(interceptors);

        try {
            runnable.run();
        } finally {
            restTemplate.setInterceptors(originalInterceptors);
        }
    }
}
