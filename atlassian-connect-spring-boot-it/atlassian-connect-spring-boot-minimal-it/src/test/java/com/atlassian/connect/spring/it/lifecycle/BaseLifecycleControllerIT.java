package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.lifecycle.LifecycleAuthenticator;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import jakarta.servlet.http.HttpServletRequest;

import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createHarmonizedLifecycleJson;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJsonWithCapabilitySet;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public abstract class BaseLifecycleControllerIT extends BaseApplicationIT {

    public MockHttpServletRequestBuilder postInstalled(String path, String secret) throws Exception {
        return postLifecycleCallback(path, "installed", secret);
    }

    public MockHttpServletRequestBuilder postInstalledWithCapabilitySet(String path, String capabilitySet) throws Exception {
        return postLifecycleCallbackWithCapabilitySet(path, "installed", capabilitySet);
    }

    public MockHttpServletRequestBuilder harmonizedPostInstalled(String path, String installationId) throws Exception {
        return harmonizedPostLifecycleCallback(path, "installed", installationId);
    }

    public MockHttpServletRequestBuilder postUninstalled(String path, String secret) throws Exception {
        return postLifecycleCallback(path, "uninstalled", secret);
    }

    public void mockAsymmetricAuthentication(LifecycleAuthenticator lifecycleAuthenticator, AtlassianHost host) {
        doAnswer(invocation -> {
            setJwtAuthenticatedPrincipal(host);
            return null;
        }).when(lifecycleAuthenticator).setAuthentication(any(HttpServletRequest.class), any(String.class));
    }

    public void mockAsymmetricAuthentication(LifecycleAuthenticator lifecycleAuthenticator, AtlassianHost host, String userAccountId) {
        doAnswer(invocation -> {
            setJwtAuthenticatedPrincipal(host, userAccountId);
            return null;
        }).when(lifecycleAuthenticator).setAuthentication(any(HttpServletRequest.class), any(String.class));
    }

    private MockHttpServletRequestBuilder postLifecycleCallback(String path, String eventType, String secret) throws JsonProcessingException {
        final String lifecycleJson = createLifecycleJson(eventType, secret);
        return postLifecycleCallbackEvent(path, lifecycleJson);
    }

    private MockHttpServletRequestBuilder postLifecycleCallbackWithCapabilitySet(String path, String eventType, String capabilitySet) throws JsonProcessingException {
        final String lifecycleJson = createLifecycleJsonWithCapabilitySet(eventType, capabilitySet);
        return postLifecycleCallbackEvent(path, lifecycleJson);
    }

    private MockHttpServletRequestBuilder harmonizedPostLifecycleCallback(String path, String eventType, String installationId) throws JsonProcessingException {
        final String lifecycleJson = createHarmonizedLifecycleJson(eventType, installationId);
        return postLifecycleCallbackEvent(path, lifecycleJson);
    }

    private MockHttpServletRequestBuilder postLifecycleCallbackEvent(String path, String lifecycleJson) {
        return post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content(lifecycleJson);
    }
}
