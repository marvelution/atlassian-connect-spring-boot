package com.atlassian.connect.spring.it.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.FixedJwtSigningClientHttpRequestInterceptor;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class SelfAuthenticationTokenVerificationIT extends BaseApplicationIT {

    private static final String SUBJECT = "cab9a26e-56ec-49e9-a08f-d7e4a19bde55";

    @Autowired
    private SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void shouldReturnPrincipalDetailsForValidSelfAuthenticationTokenHeader() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHostUser hostUser = AtlassianHostUser.builder(host).withUserAccountId(SUBJECT).build();
        final String selfAuthenticationToken = selfAuthenticationTokenGenerator.createSelfAuthenticationToken(hostUser);
        TestRestTemplate restTemplate =
                new SimpleJwtSigningRestTemplate(new FixedJwtSigningClientHttpRequestInterceptor(selfAuthenticationToken));

        ResponseEntity<AtlassianHostUser> response = restTemplate.getForEntity(getRequestUri(), AtlassianHostUser.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserAccountId().get(), is(SUBJECT));
    }

    @Test
    void shouldReturnPrincipalDetailsForValidSelfAuthenticationTokenQueryParameter() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHostUser hostUser = AtlassianHostUser.builder(host).withUserAccountId(SUBJECT).build();
        final String selfAuthenticationToken = selfAuthenticationTokenGenerator.createSelfAuthenticationToken(hostUser);

        UriComponentsBuilder requestUriBuilder = getRequestUriBuilder();
        URI requestUri = requestUriBuilder.queryParam("jwt", selfAuthenticationToken).build().toUri();

        ResponseEntity<AtlassianHostUser> response = restTemplate.getForEntity(requestUri, AtlassianHostUser.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserAccountId().get(), is(SUBJECT));
    }

    private URI getRequestUri() {
        return getRequestUriBuilder().build().toUri();
    }

    private UriComponentsBuilder getRequestUriBuilder() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress() + "/context"));
    }

    @TestConfiguration
    public static class SelfAuthenticationTokenVerificationConfiguration {

        @Bean
        public JwtVerificationIT.JwtPrincipalController jwtPrincipalController() {
            return new JwtVerificationIT.JwtPrincipalController();
        }
    }
}
