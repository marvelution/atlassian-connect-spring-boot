package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.lifecycle.LifecycleAuthenticator;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.BASE_URL;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class LifecycleControllerUnknownFieldsIT extends BaseLifecycleControllerIT {
    @MockBean
    private LifecycleAuthenticator lifecycleAuthenticator;

    @Test
    void shouldIgnoreUnexpectedFields() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).build();
        mockAsymmetricAuthentication(lifecycleAuthenticator, host);
        mvc.perform(postInstalled("/installed", SHARED_SECRET))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = hostRepository.findById(CLIENT_KEY).get();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(SHARED_SECRET));
        assertThat(installedHost.getBaseUrl(), is(BASE_URL));
        assertThat(installedHost.isAddonInstalled(), is(true));
    }

    @TestConfiguration
    public static class MockJacksonConfiguration {

        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }
    }
}
