package com.atlassian.connect.spring.it.data;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostMapping;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.AtlassianHostMappingBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class AtlassianHostMappingRepositoryIT extends BaseApplicationIT {

    @Transactional
    @Test
    void shouldDeleteMultipleInstallationIdsByClientKey() {
        AtlassianHost host = new AtlassianHostBuilder()
                .clientKey(CLIENT_KEY)
                .buildWithInstallationId();
        hostRepository.save(host);

        List<String> installationIds = Arrays.asList(
                "ari-installation-id-1",
                "ari-installation-id-2",
                "ari-installation-id-3",
                "ari-installation-id-4",
                "ari-installation-id-5"
        );

        List<AtlassianHostMapping> mappings = installationIds
                .stream()
                .map(id -> new AtlassianHostMappingBuilder()
                        .clientKey(CLIENT_KEY)
                        .installationId(id)
                        .build()
                )
                .toList();

        hostMappingRepository.saveAll(mappings);

        long deleteCount = hostMappingRepository.deleteByClientKey(CLIENT_KEY);
        assertThat(deleteCount, equalTo((long) installationIds.size()));
    }
}
