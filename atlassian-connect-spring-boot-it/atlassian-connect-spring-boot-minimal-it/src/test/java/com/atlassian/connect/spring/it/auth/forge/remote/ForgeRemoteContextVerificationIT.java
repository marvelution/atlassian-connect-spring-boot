package com.atlassian.connect.spring.it.auth.forge.remote;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostMapping;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ForgeApp;
import com.atlassian.connect.spring.ForgeRemote;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.springtest.MockServerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


@MockServerTest
@TestPropertySource(properties = {"app.id=some.app.id"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ForgeRemoteContextVerificationIT extends BaseApplicationIT {
    private MockServerClient mockServerClient;
    private RSAKey rsaJWK;

    @Autowired
    private AtlassianConnectProperties properties;
    private String INSTALLATION_ID = "some-installation-id";
    private String CLIENT_KEY = "some-client-key";
    private String BASE_URL = "http://some-base-url.com";


    @BeforeEach
    void setUp() throws JOSEException {
        properties.setForgeInvocationTokenJwksUrl("http://localhost:" + mockServerClient.getPort());
        rsaJWK = new RSAKeyGenerator(2048)
                .algorithm(new Algorithm("RS256"))
                .keyID("some-key-id")
                .keyUse(KeyUse.SIGNATURE)
                .generate();
        mockServerClient.when(request().withMethod("GET")).respond(response().withStatusCode(200).withBody("{ \"keys\": [ " + rsaJWK.toPublicJWK().toJSONString() + " ] }"));
        AtlassianHost atlassianHost = new AtlassianHost();
        atlassianHost.setClientKey(CLIENT_KEY);
        atlassianHost.setSharedSecret("11111111111111111111111111111111");
        atlassianHost.setBaseUrl(BASE_URL);
        hostRepository.save(atlassianHost);

        AtlassianHostMapping hostMapping = new AtlassianHostMapping();
        hostMapping.setClientKey(CLIENT_KEY);
        hostMapping.setInstallationId(INSTALLATION_ID);
        hostMappingRepository.save(hostMapping);
    }

    @Test
    void shouldReturn401IfAnnotationPresentButNoForgeInvocationTokenInRequest() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(URI.create(getServerAddress() + "/frc"), null, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @Test
    void shouldReturn200IfAnnotationPresentAndForgeInvocationTokenInRequest() throws JOSEException {
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.set("Authorization", "bearer " + generateToken());
        final HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = new TestRestTemplate().postForEntity(URI.create(getServerAddress() + "/frc"), entity, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertNull(response.getBody());
    }

    @Test
    void shouldReturn200IfAnnotationMarkedAsAssociateConnect() throws JOSEException {
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.set("Authorization", "bearer " + generateToken());
        final HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = new TestRestTemplate().postForEntity(URI.create(getServerAddress() + "/frc-associate-connect"), entity, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertNotNull(response.getBody());
        assertEquals(CLIENT_KEY, response.getBody());
    }

    @Test
    void shouldReturn200IfAnnotationMarkedAsAssociateConnectAndNoConnectUser() throws JOSEException {
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.set("Authorization", "bearer " + generateToken());
        final HttpEntity<String> entity = new HttpEntity<>(null, headers);

        hostMappingRepository.deleteAll();
        ResponseEntity<String> response = new TestRestTemplate().postForEntity(URI.create(getServerAddress() + "/frc-associate-connect"), entity, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    void shouldReturn401IfAnnotationAbsentAndForgeInvocationTokenInRequest() throws JOSEException {
        mockServerClient.when(request().withMethod("GET")).respond(response().withStatusCode(200).withBody("{ \"keys\": [ " + rsaJWK.toPublicJWK().toJSONString() + " ] }"));

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.set("Authorization", "bearer " + generateToken());
        final HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = new TestRestTemplate().postForEntity(URI.create(getServerAddress() + "/non-frc"), entity, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    }

    @TestConfiguration
    public static class ForgeRemoteControllerConfiguration {

        @Bean
        public ForgeRemoteContextVerificationIT.ForgeRemoteController testForgeRemoteController() {
            return new ForgeRemoteContextVerificationIT.ForgeRemoteController();
        }
    }

    @RestController
    public static class ForgeRemoteController {

        @ForgeRemote
        @PostMapping(value = "/frc")
        @ResponseBody
        public String forgeRemoteContextTestMethod(@AuthenticationPrincipal AtlassianHostUser hostUser) {
            if (hostUser == null) {
                return "";
            }
            return hostUser.getHost().getClientKey();
        }

        @ForgeRemote(associateConnect = true)
        @PostMapping(value = "/frc-associate-connect")
        @ResponseBody
        public String forgeRemoteContextAssociateConnectTestMethod(@AuthenticationPrincipal AtlassianHostUser hostUser) {
            if (hostUser == null) {
                return "";
            }
            return hostUser.getHost().getClientKey();
        }

        @PostMapping(value = "/non-frc")
        @ResponseBody
        public String nonForgeRemoteContextTestMethod() {
            return "ok";
        }
    }

    private String generateToken() throws JOSEException {
        ForgeApp forgeApp = new ForgeApp();
        forgeApp.setApiBaseUrl(BASE_URL);
        forgeApp.setInstallationId(INSTALLATION_ID);
        forgeApp.setId("some-app-id");

        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .claim("app", forgeApp)
                .issuer("forge/invocation-token")
                .audience("some.app.id")
                .subject("forge/invocation-token")
                .expirationTime(new Date(new Date().getTime() + 60 * 1000))
                .build();

        JWSSigner signer = new RSASSASigner(rsaJWK);
        SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(rsaJWK.getKeyID()).build(), claimsSet);
        signedJWT.sign(signer);

        return signedJWT.serialize();
    }
}
