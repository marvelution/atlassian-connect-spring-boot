package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.asymmetric.AsymmetricPublicKeyProvider;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.AsymmetricKeys;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.LifecycleBodyHelper;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import com.nimbusds.jose.JOSEException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DevelopmentLifecycleControllerIT extends BaseApplicationIT {

    private static final URI PRODUCTION_INSTALL_KEYS_URL = URI.create("https://example.keys.com");

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianConnectProperties properties;

    @Autowired
    private AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;

    @BeforeEach
    public void before() throws JOSEException {
        AsymmetricKeys.generateKeys(UUID.randomUUID().toString());
        properties.setPublicKeyBaseUrl(PRODUCTION_INSTALL_KEYS_URL.toString());
        RestTemplate restTemplate = asymmetricPublicKeyProvider.getAsymmetricPublicKeyProviderRestTemplate();
        MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);
        mockServer.expect(requestTo(PRODUCTION_INSTALL_KEYS_URL + "/" + AsymmetricKeys.getKeyId())).andRespond(withSuccess(AsymmetricKeys.getPublicKey(), MediaType.TEXT_PLAIN));
    }

    @Test
    void shouldAcceptInstallFromUnknownHostInDevMode() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().build();
        String appBaseUrl = addonDescriptorLoader.getDescriptor().getBaseUrl();
        TestRestTemplate restTemplate = SimpleJwtSigningRestTemplate.asymmetricJwtSigningRestTemplate(host, appBaseUrl, AsymmetricKeys.getPrivateKey(), AsymmetricKeys.getKeyId());
        ResponseEntity<String> response = restTemplate.postForEntity(URI.create(getServerAddress() + "/installed"),
                LifecycleBodyHelper.createLifecycleEventMap("installed"), String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }
}
