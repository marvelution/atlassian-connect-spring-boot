package com.atlassian.connect.spring.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinimalAddonApplication {

    public static void main(String[] args) {
        new SpringApplication(MinimalAddonApplication.class).run(args);
    }
}
