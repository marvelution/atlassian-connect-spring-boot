package com.atlassian.connect.spring.it.util;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.read.ListAppender;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.slf4j.Logger.ROOT_LOGGER_NAME;

public class LogbackCaptureExtension implements BeforeEachCallback, AfterEachCallback {

    private ListAppender<ILoggingEvent> listAppender;
    private Logger logger;

    private LoggerContext loggerContext;


    @Override
    public void beforeEach(ExtensionContext extensionContext) {
        loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.reset();
        listAppender = new ListAppender<>();
        logger = (Logger) LoggerFactory.getLogger(ROOT_LOGGER_NAME);
        logger.addAppender(listAppender);
        listAppender.start();

    }

    @Override
    public void afterEach(ExtensionContext extensionContext) throws Exception {
        listAppender.stop();
        loggerContext.reset();
        logger.detachAppender(listAppender);
        new ContextInitializer(loggerContext).autoConfig();
    }

    public List<ILoggingEvent> getEvents() {
        if (listAppender == null) {
            throw new RuntimeException("Register the LogBackCapture extension");
        }
        return listAppender.list;
    }
}
