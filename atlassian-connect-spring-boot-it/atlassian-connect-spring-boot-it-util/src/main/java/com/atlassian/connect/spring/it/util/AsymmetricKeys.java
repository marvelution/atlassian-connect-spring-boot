package com.atlassian.connect.spring.it.util;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;

import java.nio.charset.StandardCharsets;
import java.security.interfaces.RSAPrivateKey;
import java.util.Base64;
import java.util.UUID;


public class AsymmetricKeys {

    public static final String BEGIN = "-----BEGIN PUBLIC KEY-----";
    public static final String END = "-----END PUBLIC KEY-----";
    private static RSAKey rsaKey;

    static {
        try {
            generateKeys(UUID.randomUUID().toString());
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        }
    }

    private AsymmetricKeys() {
    }

    public static void generateKeys(String keyId) throws JOSEException {
        rsaKey = new RSAKeyGenerator(2048)
                .keyUse(KeyUse.SIGNATURE)
                .keyID(keyId)
                .generate();
    }

    public static RSAPrivateKey getPrivateKey() throws JOSEException {
        if (rsaKey == null) {
            generateKeys(UUID.randomUUID().toString());
        }
        return rsaKey.toRSAPrivateKey();
    }

    public static String getPublicKey() throws JOSEException {

        if (rsaKey == null) {
            generateKeys(UUID.randomUUID().toString());
        }

        byte[] data = rsaKey.toRSAPublicKey().getEncoded();
        Base64.Encoder encoder = Base64.getMimeEncoder(64, System.lineSeparator().getBytes(StandardCharsets.UTF_8));
        String encoded = encoder.encodeToString(data);

        return BEGIN + System.lineSeparator() + encoded + System.lineSeparator() + END;
    }

    public static String getKeyId() throws JOSEException {
        if (rsaKey == null) {
            generateKeys(UUID.randomUUID().toString());
        }
        return rsaKey.getKeyID();
    }
}
