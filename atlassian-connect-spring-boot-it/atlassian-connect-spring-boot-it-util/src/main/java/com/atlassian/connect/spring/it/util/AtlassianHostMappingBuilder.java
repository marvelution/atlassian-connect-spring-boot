package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AddonAuthenticationType;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostMapping;

public class AtlassianHostMappingBuilder {

    private String clientKey = AtlassianHosts.CLIENT_KEY;

    private String installationId = AtlassianHosts.INSTALLATION_ID;

    public AtlassianHostMappingBuilder() {
    }

    public AtlassianHostMappingBuilder clientKey(String clientKey) {
        this.clientKey = clientKey;
        return this;
    }

    public AtlassianHostMappingBuilder installationId(String installationId) {
        this.installationId = installationId;
        return this;
    }

    public AtlassianHostMapping build() {
        AtlassianHostMapping mapping = new AtlassianHostMapping();
        mapping.setClientKey(clientKey);
        mapping.setInstallationId(installationId);
        return mapping;
    }
}
