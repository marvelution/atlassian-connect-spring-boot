package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AddonAuthenticationType;

public class AtlassianHostBuilder {

    private String clientKey = AtlassianHosts.CLIENT_KEY;

    private String oauthClientId = AtlassianHosts.OAUTH_CLIENT_ID;

    private String cloudId = AtlassianHosts.CLOUD_ID;

    private AddonAuthenticationType authenticationType = AddonAuthenticationType.JWT;

    private String sharedSecret = AtlassianHosts.SHARED_SECRET;

    private String baseUrl = AtlassianHosts.BASE_URL;

    private String displayUrl = AtlassianHosts.BASE_URL;

    private String displayUrlServicedeskHelpCenter = AtlassianHosts.BASE_URL;

    private String productType = AtlassianHosts.PRODUCT_TYPE;

    private String description = "Test host";

    private String serviceEntitlementNumber = null;

    private String entitlementId = null;

    private String entitlementNumber = null;

    private String installationId = AtlassianHosts.INSTALLATION_ID;

    private boolean addonInstalled = true;

    private String capabilitySet = null;

    public AtlassianHostBuilder() {
    }

    public AtlassianHostBuilder clientKey(String clientKey) {
        this.clientKey = clientKey;
        return this;
    }

    public AtlassianHostBuilder oauthClientId(String oauthClientId) {
        this.oauthClientId = oauthClientId;
        return this;
    }

    public AtlassianHostBuilder cloudId(String cloudId) {
        this.cloudId = cloudId;
        return this;
    }

    public AtlassianHostBuilder authenticationType(AddonAuthenticationType authenticationType) {
        this.authenticationType = authenticationType;
        return this;
    }

    public AtlassianHostBuilder sharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
        return this;
    }

    public AtlassianHostBuilder baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public AtlassianHostBuilder displayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
        return this;
    }

    public AtlassianHostBuilder displayUrlServicedeskHelpCenter(String displayUrlServicedeskHelpCenter) {
        this.displayUrlServicedeskHelpCenter = displayUrlServicedeskHelpCenter;
        return this;
    }

    public AtlassianHostBuilder productType(String productType) {
        this.productType = productType;
        return this;
    }

    public AtlassianHostBuilder description(String description) {
        this.description = description;
        return this;
    }

    public AtlassianHostBuilder serviceEntitlementNumber(String serviceEntitlementNumber) {
        this.serviceEntitlementNumber = serviceEntitlementNumber;
        return this;
    }

    public AtlassianHostBuilder entitlementId(String entitlementId) {
        this.entitlementId = entitlementId;
        return this;
    }

    public AtlassianHostBuilder entitlementNumber(String entitlementNumber) {
        this.entitlementNumber = entitlementNumber;
        return this;
    }

    public AtlassianHostBuilder addonInstalled(boolean addonInstalled) {
        this.addonInstalled = addonInstalled;
        return this;
    }

    public AtlassianHostBuilder capabilitySet(String capabilitySet) {
        this.capabilitySet = capabilitySet;
        return this;
    }

    public AtlassianHost build() {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(clientKey);
        host.setOauthClientId(oauthClientId);
        host.setSharedSecret(sharedSecret);
        host.setBaseUrl(baseUrl);
        host.setDisplayUrl(displayUrl);
        host.setDisplayUrlServicedeskHelpCenter(displayUrlServicedeskHelpCenter);
        host.setProductType(productType);
        host.setDescription(description);
        host.setServiceEntitlementNumber(serviceEntitlementNumber);
        host.setAddonInstalled(addonInstalled);
        return host;
    }

    public AtlassianHost buildWithCloudIdAndAuthenticationType() {
        AtlassianHost host = this.build();
        host.setCloudId(cloudId);
        host.setAuthentication(authenticationType);
        return host;
    }

    public AtlassianHost buildWithInstallationId() {
        AtlassianHost host = this.build();
        host.setInstallationId(installationId);
        return host;
    }
}
