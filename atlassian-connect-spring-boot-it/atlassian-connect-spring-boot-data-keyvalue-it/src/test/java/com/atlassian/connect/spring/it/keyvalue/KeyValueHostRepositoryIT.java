package com.atlassian.connect.spring.it.keyvalue;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class KeyValueHostRepositoryIT {

    @Autowired
    private AtlassianHostRepository hostRepository;

    @AfterEach
    public void tearDown() {
        hostRepository.deleteAll();
    }

    @Test
    void shouldHostRepositoryBeEmpty() {
        assertThat(hostRepository.count(), is(0L));
    }

    @Test
    void shouldHostRepositoryHaveOneHostAfterSavingOne() {
        createAndSaveHost(hostRepository);
        assertThat(hostRepository.count(), is(1L));
    }

    @Test
    void shouldFindSavedHostByClientKey() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHost foundHost = hostRepository.findById(host.getClientKey()).get();
        assertThat(foundHost, equalTo(host));
    }
}
