package com.atlassian.connect.spring.it.keyvalue;

import com.atlassian.connect.spring.AtlassianHostRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.map.repository.config.EnableMapRepositories;

@SpringBootApplication
@EnableMapRepositories(basePackageClasses = {AtlassianHostRepository.class})
public class KeyValueAddonApplication {

    public static void main(String[] args) {
        new SpringApplication(KeyValueAddonApplication.class).run(args);
    }
}
