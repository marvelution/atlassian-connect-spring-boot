package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.springframework.security.oauth2.client.endpoint.AbstractOAuth2AuthorizationGrantRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.util.Assert;

public class OAuth2JwtAssertionAuthorizationGrantRequest extends AbstractOAuth2AuthorizationGrantRequest {

    private final ClientRegistration clientRegistration;

    private final AtlassianHostUser hostUser;

    /**
     * Constructs an {@code OAuth2JwtAssertionAuthorizationGrantRequest} using the provided parameters.
     *
     * @param clientRegistration the client registration
     */
    public OAuth2JwtAssertionAuthorizationGrantRequest(ClientRegistration clientRegistration, AtlassianHostUser hostUser) {
        super(OAuth2JwtClientRegistrationRepository.JWT_BEARER_GRANT_TYPE, clientRegistration);
        this.hostUser = hostUser;
        Assert.notNull(clientRegistration, "clientRegistration cannot be null");
        Assert.isTrue(OAuth2JwtClientRegistrationRepository.JWT_BEARER_GRANT_TYPE.equals(clientRegistration.getAuthorizationGrantType()),
                "clientRegistration.authorizationGrantType must be urn:ietf:params:oauth:grant-type:jwt-bearer");
        this.clientRegistration = clientRegistration;
    }

    /**
     * Returns the {@link ClientRegistration client registration}.
     *
     * @return the {@link ClientRegistration}
     */
    @Override
    public ClientRegistration getClientRegistration() {
        return clientRegistration;
    }

    public AtlassianHostUser getHostUser() {
        return hostUser;
    }
}
