package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * A helper class for resolving URLs relative to the base URL of an {@link AtlassianHost}.
 */
@Component
public class AtlassianHostUriResolver {
    private static final Logger log = LoggerFactory.getLogger(AtlassianHostUriResolver.class);

    private final AtlassianHostRepository hostRepository;

    private final AtlassianRequestUtils requestUtils;

    @Autowired
    public AtlassianHostUriResolver(AtlassianHostRepository hostRepository,
                                    AtlassianRequestUtils requestUtils) {
        this.hostRepository = hostRepository;
        this.requestUtils = requestUtils;
    }

    public static boolean isRequestToHost(URI requestUri, AtlassianHost host) {
        URI hostBaseUri = URI.create(host.getBaseUrl());
        return !hostBaseUri.relativize(requestUri).isAbsolute();
    }

    public static boolean isRequestToAtlassianGateway(URI requestUri) {
        if (requestUri.isAbsolute()) {
            String host = requestUri.getHost();
            return host.equals(AtlassianRequestUtils.PRODUCTION_API_GATEWAY_HOST_URL) || host.equals(AtlassianRequestUtils.STAGING_API_GATEWAY_URL_HOST_URL);
        }
        return false;
    }

    public static String getBaseUrl(URI uri) {
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), null, null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }

    public Optional<AtlassianHost> getHostFromRequestUrl(URI uri) {
        Optional<AtlassianHost> optionalHost = Optional.empty();
        if (uri.isAbsolute()) {
            optionalHost = getHostFromBaseUrl(AtlassianHostUriResolver.getBaseUrl(uri));
            if (optionalHost.isEmpty()) {
                optionalHost = getHostFromBaseUrl(getBaseUrlWithFirstPathElement(uri));
            }
        }

        if (optionalHost.isPresent()) {
            log.warn("Host look-up by base URL is deprecated");
        }

        return optionalHost;
    }

    public URI resolveToAbsoluteUriWithApiGateway(URI uri, AtlassianHost host) {
        String productType = host.getProductType();
        String cloudId = host.getCloudId();

        if (isRequestToAtlassianGateway(uri)) {
            return uri;
        }

        String pathPrefix = this.requestUtils.getApiGatewayUrlPrefix(host) + '/' + productType + '/' + cloudId;
        return resolveToAbsoluteUriWithBase(uri, pathPrefix);
    }

    public URI resolveToAbsoluteUriWithBase(URI uri, String baseUriString) {
        URI baseUri = URI.create(baseUriString);
        return baseUri.resolve(requestUtils.getRelativeUriToResolve(uri, baseUri));
    }

    @SuppressWarnings("deprecation")
    private Optional<AtlassianHost> getHostFromBaseUrl(String baseUrl) {
        return hostRepository.findFirstByBaseUrlOrderByLastModifiedDateDesc(baseUrl);
    }

    private String getBaseUrlWithFirstPathElement(URI uri) {
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), getFirstPathElement(uri), null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }

    private String getFirstPathElement(URI uri) {
        String path = uri.getPath();
        if (path != null) {
            int secondSlashIndex = path.indexOf('/', 1);
            if (secondSlashIndex != -1) {
                path = path.substring(0, secondSlashIndex);
            }
        }
        return path;
    }
}
