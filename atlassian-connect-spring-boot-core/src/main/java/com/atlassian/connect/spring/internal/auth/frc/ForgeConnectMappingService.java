package com.atlassian.connect.spring.internal.auth.frc;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostMapping;
import com.atlassian.connect.spring.AtlassianHostMappingRepository;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ForgeInvocationToken;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ForgeConnectMappingService {
    private final AtlassianHostMappingRepository atlassianHostMappingRepository;

    private final AtlassianHostRepository atlassianHostRepository;

    public ForgeConnectMappingService(AtlassianHostMappingRepository atlassianHostMappingRepository, AtlassianHostRepository atlassianHostRepository) {
        this.atlassianHostMappingRepository = atlassianHostMappingRepository;
        this.atlassianHostRepository = atlassianHostRepository;
    }

    public Optional<AtlassianHostUser> getAtlassianHostUserFromForgeInvocationToken(ForgeInvocationToken forgeInvocationToken) {
        Optional<AtlassianHostMapping> hostMapping = atlassianHostMappingRepository.findById(forgeInvocationToken.getApp().getInstallationId());
        if (hostMapping.isEmpty()) {
            return Optional.empty();
        }
        AtlassianHostMapping atlassianHostMapping = hostMapping.get();
        String clientKey = atlassianHostMapping.getClientKey();
        Optional<AtlassianHost> atlassianHost = atlassianHostRepository.findById(clientKey);
        if (atlassianHost.isEmpty()) {
            return Optional.empty();
        }
        AtlassianHostUser.AtlassianHostUserBuilder atlassianHostUserBuilder = AtlassianHostUser.builder(atlassianHost.get());
        String userAccountId = forgeInvocationToken.getPrincipal();
        Optional.ofNullable(userAccountId).ifPresent(atlassianHostUserBuilder::withUserAccountId);
        AtlassianHostUser hostUser = atlassianHostUserBuilder.build();
        return Optional.ofNullable(hostUser);
    }

}
