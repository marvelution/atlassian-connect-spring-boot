package com.atlassian.connect.spring.internal.auth;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.jwt.JwtCredentials;
import com.atlassian.connect.spring.internal.auth.jwt.UnknownJwtIssuerException;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.CanonicalRequestUtil;
import com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Base AuthenticationProvider class for JWT authentication
 */
public abstract class AbstractConnectAuthenticationProvider implements AuthenticationProvider {
    protected static final Logger log = LoggerFactory.getLogger(AbstractConnectAuthenticationProvider.class);
    protected final AddonDescriptorLoader addonDescriptorLoader;
    protected final AtlassianHostRepository hostRepository;

    protected AbstractConnectAuthenticationProvider(AddonDescriptorLoader addonDescriptorLoader, AtlassianHostRepository hostRepository) {
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.hostRepository = hostRepository;
    }

    protected AtlassianHostUser createHostUserFromSubjectClaim(AtlassianHost host, JWTClaimsSet verifiedClaims) {
        String userAccountId = verifiedClaims.getSubject();
        AtlassianHostUser.AtlassianHostUserBuilder builder = AtlassianHostUser.builder(host);

        Optional.ofNullable(userAccountId).ifPresent(builder::withUserAccountId);

        return builder.build();
    }

    protected String computeQueryStringHash(JwtCredentials jwtCredentials) {
        CanonicalHttpRequest canonicalHttpRequest = jwtCredentials.getCanonicalHttpRequest();
        if (log.isDebugEnabled()) {
            log.debug("Canonical request for incoming JWT: {}", CanonicalRequestUtil.toVerboseString(canonicalHttpRequest));
        }

        try {
            return HttpRequestCanonicalizer.computeCanonicalRequestHash(canonicalHttpRequest);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    protected Optional<String> getHostClientKeyFromSelfAuthenticationToken(JWTClaimsSet unverifiedClaims) {
        Optional<String> optionalClientKey = Optional.empty();
        String addonKey = addonDescriptorLoader.getDescriptor().getKey();
        if (addonKey.equals(unverifiedClaims.getIssuer())) {
            assertValidSelfAuthenticationTokenAudience(unverifiedClaims, addonKey);

            Object clientKeyClaim = unverifiedClaims.getClaim(SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM);
            String clientKey = assertValidSelfAuthenticationTokenClientKey(clientKeyClaim);
            optionalClientKey = Optional.of(clientKey);
        }
        return optionalClientKey;
    }

    private void assertValidSelfAuthenticationTokenAudience(JWTClaimsSet unverifiedClaims, String addonKey) {
        List<String> audience = unverifiedClaims.getAudience();
        if (audience == null) {
            throw new BadCredentialsException("Missing audience for self-authentication token");
        }

        if (!audience.equals(Collections.singletonList(addonKey))) {
            throw new BadCredentialsException(String.format("Invalid audience (%s) for self-authentication token", String.join(",", audience)));
        }
    }

    private String assertValidSelfAuthenticationTokenClientKey(Object clientKeyClaim) {
        if (clientKeyClaim == null) {
            throw new BadCredentialsException("Missing client key claim for self-authentication token");
        }
        return clientKeyClaim.toString();
    }

    protected AtlassianHost getHost(String clientKey) throws AuthenticationException {
        return hostRepository.findById(clientKey).orElseThrow(() -> {
            UsernameNotFoundException usernameNotFoundException = new UnknownJwtIssuerException(clientKey);
            log.debug(usernameNotFoundException.getMessage());
            return usernameNotFoundException;
        });
    }
}
