package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class OAuth2JwtTokenService {

    private static final Logger log = LoggerFactory.getLogger(OAuth2JwtTokenService.class);

    private final OAuth2AuthorizedClientService authorizedClientService;
    private final OAuth2JwtAuthorizedClientProvider authorizedClientProvider;
    private final OAuth2JwtClientRegistrationRepository clientRegistrationRepository;


    @Autowired
    public OAuth2JwtTokenService(OAuth2JwtAuthorizedClientProvider authorizedClientProvider, @Qualifier("oauth2-jwt-impersonation") OAuth2JwtClientRegistrationRepository clientRegistrationRepository) {
        this.authorizedClientService = new InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository);
        this.authorizedClientProvider = authorizedClientProvider;
        this.clientRegistrationRepository = clientRegistrationRepository;
    }

    public OAuth2AccessToken getAccessToken(AtlassianHostUser hostUser) {
        Authentication authentication = new OAuth2JwtAuthentication(hostUser);
        ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(hostUser.getHost().getClientKey());
        OAuth2AuthorizedClient authorizedClient = Optional.<OAuth2AuthorizedClient>ofNullable(
                        authorizedClientService.loadAuthorizedClient(clientRegistration.getRegistrationId(), authentication.getName()))
                .orElseGet(() -> this.reauthorize(clientRegistration, authentication));

        if (hasAccessTokenExpired(authorizedClient.getAccessToken())) {
            authorizedClient = this.reauthorize(clientRegistration, authentication);
        }

        if (Objects.isNull(authorizedClient)) {
            throw new IllegalStateException("authorizedClient shouldn't be null");
        }

        return authorizedClient.getAccessToken();
    }

    private OAuth2AuthorizedClient reauthorize(ClientRegistration clientRegistration, Authentication authentication) {
        OAuth2AuthorizationContext oAuth2AuthorizationContext = OAuth2AuthorizationContext.withClientRegistration(clientRegistration)
                .principal(authentication)
                .build();
        Optional<OAuth2AuthorizedClient> authorizedClient = Optional.ofNullable(authorizedClientProvider.authorize(oAuth2AuthorizationContext));
        authorizedClient.ifPresent(client -> authorizedClientService.saveAuthorizedClient(client, authentication));

        return authorizedClient.orElse(null);
    }

    private boolean hasAccessTokenExpired(OAuth2AccessToken token) {
        boolean hasTokenExpired = authorizedClientProvider.willTokenExpire(token);
        if (hasTokenExpired) {
            log.info("Refreshing expired access token");
        }

        return hasTokenExpired;
    }

    /**
     * Visible for testing
     *
     * @return authorizedClientService
     */
    public OAuth2AuthorizedClientService getoAuth2AuthorizedClientService() {
        return this.authorizedClientService;
    }
}
