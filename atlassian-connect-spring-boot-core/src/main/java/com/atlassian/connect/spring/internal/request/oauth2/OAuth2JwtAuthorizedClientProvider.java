package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.AtlassianRequestUtils;
import jakarta.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import static com.atlassian.connect.spring.internal.request.oauth2.OAuth2JwtClientRegistrationRepository.JWT_BEARER_GRANT_TYPE;

@Component
public class OAuth2JwtAuthorizedClientProvider implements OAuth2AuthorizedClientProvider {

    private static final Logger log = LoggerFactory.getLogger(OAuth2JwtAuthorizedClientProvider.class);

    private final AtlassianConnectSecurityContextHelper securityContextHelper;

    private OAuth2AccessTokenResponseClient<OAuth2JwtAssertionAuthorizationGrantRequest> accessTokenResponseClient;

    private Duration clockSkew = Duration.ofSeconds(60);

    private Clock clock = Clock.systemUTC();

    public OAuth2JwtAuthorizedClientProvider(AtlassianConnectSecurityContextHelper securityContextHelper,
                                             OAuth2JwtAccessTokenResponseClient accessTokenResponseClient) {
        this.securityContextHelper = securityContextHelper;
        this.accessTokenResponseClient = accessTokenResponseClient;
    }

    @Override
    @Nullable
    public OAuth2AuthorizedClient authorize(OAuth2AuthorizationContext context) {
        Assert.notNull(context, "context cannot be null");

        ClientRegistration clientRegistration = context.getClientRegistration();
        OAuth2AuthorizedClient authorizedClient = context.getAuthorizedClient();
        AuthorizationGrantType grantType = clientRegistration.getAuthorizationGrantType();

        AtlassianRequestUtils.validateAuthorizationGrantType(grantType, JWT_BEARER_GRANT_TYPE);
        log.debug("Requesting OAuth 2.0 access token using {}", grantType);

        if (authorizedClient != null && !willTokenExpire(authorizedClient.getAccessToken())) {
            // If client is already authorized and access token is NOT expired than no need for re-authorization
            return null;
        }

        AtlassianHostUser hostUser = securityContextHelper.getHostUserFromAuthentication(context.getPrincipal())
                .orElseThrow(() -> new IllegalStateException("No current user found"));
        OAuth2JwtAssertionAuthorizationGrantRequest authorizationGrantRequest = new OAuth2JwtAssertionAuthorizationGrantRequest(clientRegistration, hostUser);
        OAuth2AccessTokenResponse tokenResponse = this.accessTokenResponseClient.getTokenResponse(authorizationGrantRequest);
        return new OAuth2AuthorizedClient(clientRegistration, context.getPrincipal().getName(), tokenResponse.getAccessToken());
    }

    /**
     * Sets the client used when requesting an access token credential at the Token Endpoint.
     *
     * @param accessTokenResponseClient the client used when requesting an access token credential at the Token Endpoint
     */
    public void setAccessTokenResponseClient(OAuth2AccessTokenResponseClient<OAuth2JwtAssertionAuthorizationGrantRequest> accessTokenResponseClient) {
        Assert.notNull(accessTokenResponseClient, "accessTokenResponseClient cannot be null");
        this.accessTokenResponseClient = accessTokenResponseClient;
    }

    /**
     * Sets the maximum acceptable clock skew, which is used when checking the
     * {@link OAuth2AuthorizedClient#getAccessToken() access token} expiry. The default is 60 seconds.
     * An access token is considered expired if it's before {@code Instant.now(this.clock) - clockSkew}.
     *
     * @param clockSkew the maximum acceptable clock skew
     */
    public void setClockSkew(Duration clockSkew) {
        Assert.notNull(clockSkew, "clockSkew cannot be null");
        Assert.isTrue(clockSkew.getSeconds() >= 0, "clockSkew must be >= 0");
        this.clockSkew = clockSkew;
    }

    /**
     * Sets the {@link Clock} used in {@link Instant#now(Clock)} when checking the access token expiry.
     *
     * @param clock the clock
     */
    public void setClock(Clock clock) {
        Assert.notNull(clock, "clock cannot be null");
        this.clock = clock;
    }

    /**
     * Indicates if the given token has expired or is likely to expire when it gets to the server.
     * It allows for some clock skew
     *
     * @param token the token being checked
     * @return true if the token has expired or is likely to have expired when it reaches the server
     */
    public boolean willTokenExpire(AbstractOAuth2Token token) {
        Instant expiresAt = token.getExpiresAt();
        return expiresAt != null && expiresAt.isBefore(Instant.now(this.clock).plus(this.clockSkew));
    }
}
