package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.AtlassianHost;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class AtlassianConnectPerTenantEnvironmentConfiguration {
    private static final URI AUTHORIZATION_SERVER_URL_STAGING = URI.create("https://oauth-2-authorization-server.stg.services.atlassian.com");
    private static final URI AUTHORIZATION_SERVER_URL_PRODUCTION = URI.create("https://oauth-2-authorization-server.services.atlassian.com");
    private static final URI AUTHORIZATION_SERVER_URL_FEDRAMP_STAGING = URI.create("https://oauth-2-authorization-server.stg.services.atlassian-us-gov-mod.com");
    private static final URI AUTHORIZATION_SERVER_URL_FEDRAMP_PRODUCTION = URI.create("https://oauth-2-authorization-server.services.atlassian-us-gov-mod.com");

    public static final String PUBLIC_KEY_BASE_URL_STAGING = "https://cs-migrations--cdn.us-east-1.staging.public.atl-paas.net";
    public static final String PUBLIC_KEY_BASE_URL_PRODUCTION = "https://connect-install-keys.atlassian.com";
    public static final String PUBLIC_KEY_BASE_URL_FEDRAMP_STAGING = "https://cs-migrations--fedrampcdn.us-east-1.staging.cdn.atlassian-us-gov-mod.com";
    public static final String PUBLIC_KEY_BASE_URL_FEDRAMP_PRODUCTION = "https://cs-migrations--fedrampcdn.us-east-1.prod.cdn.atlassian-us-gov-mod.com";

    private final AtlassianConnectProperties properties;

    public AtlassianConnectPerTenantEnvironmentConfiguration(AtlassianConnectProperties properties) {
        this.properties = properties;
    }

    public URI getAuthorizationServerBaseUrl(AtlassianHost host) {
        String baseUrlHost = URI.create(host.getBaseUrl()).getHost();
        if (isProductionHost(baseUrlHost)) {
            return AUTHORIZATION_SERVER_URL_PRODUCTION;
        }

        if (isFedRAMPProductionHost(baseUrlHost)) {
            return AUTHORIZATION_SERVER_URL_FEDRAMP_PRODUCTION;
        }

        if (isDevHost(baseUrlHost)) {
            return AUTHORIZATION_SERVER_URL_STAGING;
        }

        if (isFedRAMPStagingHost(baseUrlHost)) {
            return AUTHORIZATION_SERVER_URL_FEDRAMP_STAGING;
        }

        // fall through to production if not recognized
        return AUTHORIZATION_SERVER_URL_PRODUCTION;
    }

    public String getPublicKeyBaseUrl(String url) {
        String hostUrl = URI.create(url).getHost();
        String publicKeyBaseUrlFromProperties = properties.getPublicKeyBaseUrl();

        if (publicKeyBaseUrlFromProperties != null) {
            return publicKeyBaseUrlFromProperties;
        }

        if (isProductionHost(hostUrl)) {
            return PUBLIC_KEY_BASE_URL_PRODUCTION;
        }

        if (isFedRAMPProductionHost(hostUrl)) {
            return PUBLIC_KEY_BASE_URL_FEDRAMP_PRODUCTION;
        }

        if (isDevHost(hostUrl)) {
            return PUBLIC_KEY_BASE_URL_STAGING;
        }

        if (isFedRAMPStagingHost(hostUrl)) {
            return PUBLIC_KEY_BASE_URL_FEDRAMP_STAGING;
        }

        // fall through to production if not recognized
        return PUBLIC_KEY_BASE_URL_PRODUCTION;
    }

    private boolean isDevHost(String host) {
        return host.endsWith(".jira-dev.com");
    }

    private boolean isFedRAMPStagingHost(String host) {
        return host.endsWith(".atlassian-stg-fedm.net");
    }

    private boolean isFedRAMPProductionHost(String host) {
        return host.endsWith(".atlassian-us-gov-mod.net");
    }

    private boolean isProductionHost(String host) {
        return host.endsWith(".atlassian.net") || host.endsWith(".jira.com");
    }
}
