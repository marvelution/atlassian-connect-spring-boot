package com.atlassian.connect.spring.internal.lifecycle;

import com.atlassian.connect.spring.AddonAuthenticationType;
import com.atlassian.connect.spring.AddonInstalledEvent;
import com.atlassian.connect.spring.AddonUninstalledEvent;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostMapping;
import com.atlassian.connect.spring.AtlassianHostMappingRepository;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.AsynchronousApplicationEventPublisher;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.AbstractAuthentication;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.JwtInvalidClaimException;
import com.nimbusds.jwt.JWTClaimsSet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import static com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer.QUERY_STRING_HASH_CLAIM_NAME;

/**
 * A controller that handles the add-on installation and uninstallation lifecycle callbacks.
 *
 * @see LifecycleControllerHandlerMapping
 */
@RestController
public class LifecycleController {

    private static final Logger log = LoggerFactory.getLogger(LifecycleController.class);

    private final AtlassianHostRepository hostRepository;

    private final AtlassianHostMappingRepository hostMappingRepository;

    private final AddonDescriptorLoader addonDescriptorLoader;

    private final LifecycleAuthenticator lifecycleAuthenticator;

    private final AsynchronousApplicationEventPublisher eventPublisher;

    private final TransactionExecutor transactionExecutor;

    private final AtlassianConnectProperties atlassianConnectProperties;

    public LifecycleController(AtlassianHostRepository hostRepository,
                               AtlassianHostMappingRepository hostMappingRepository,
                               AddonDescriptorLoader addonDescriptorLoader,
                               LifecycleAuthenticator lifecycleAuthenticator,
                               AsynchronousApplicationEventPublisher eventPublisher,
                               TransactionExecutor transactionExecutor,
                               AtlassianConnectProperties atlassianConnectProperties) {
        this.hostRepository = hostRepository;
        this.hostMappingRepository = hostMappingRepository;
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.lifecycleAuthenticator = lifecycleAuthenticator;
        this.eventPublisher = eventPublisher;
        this.transactionExecutor = transactionExecutor;
        this.atlassianConnectProperties = atlassianConnectProperties;
    }

    public static Method getInstalledMethod() {
        return getSafeMethod("installed");
    }

    public static Method getUninstalledMethod() {
        return getSafeMethod("uninstalled");
    }

    private static Method getSafeMethod(String name) {
        try {
            return LifecycleController.class.getMethod(name, HttpServletRequest.class, LifecycleEvent.class);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
    }

    public ResponseEntity<Void> installed(HttpServletRequest request, @Valid @RequestBody LifecycleEvent lifecycleEvent) {
        lifecycleAuthenticator.setAuthentication(request, lifecycleEvent.baseUrl);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AtlassianHostUser hostUser = (AtlassianHostUser) authentication.getPrincipal();

        SecurityContext context = SecurityContextHolder.getContext();  // <--- get context in the parent thread
        Supplier<ResponseEntity<Void>> installer = () -> {
            SecurityContextHolder.setContext(context);                 // <--- set context in the child thread
            return installedImpl(lifecycleEvent, hostUser, authentication);
        };
        AtomicBoolean requestTimeoutReached = new AtomicBoolean(false);
        CompletableFuture<ResponseEntity<Void>> installFuture
                = transactionExecutor.executeWithRollbackOption(installer, requestTimeoutReached);

        Integer installTimeout = atlassianConnectProperties.getInstallTimeout();
        try {
            return installFuture.get(installTimeout, TimeUnit.SECONDS);
        } catch (TimeoutException | InterruptedException e) {
            log.warn("Installation request timed out. Attempting to roll back transaction. (Timeout is {} seconds per property {}.)", installTimeout, "atlassian.connect.install-timeout");
            requestTimeoutReached.set(true);

            RuntimeException asyncRequestTimeout = new AsyncRequestTimeoutException();
            asyncRequestTimeout.initCause(e);
            throw asyncRequestTimeout;
        } catch (ExecutionException e) {
            throw new RuntimeException(e.getCause());
        }
    }

    private ResponseEntity<Void> installedImpl(LifecycleEvent lifecycleEvent, AtlassianHostUser hostUser, Authentication authentication) {
        assertExpectedEventType(lifecycleEvent, "installed");

        Optional<AtlassianHost> maybeExistingHost = getHostFromLifecycleEvent(lifecycleEvent);

        if (hostUser == null) {
            if (maybeExistingHost.isPresent()) {
                log.error("Installation request was not properly authenticated, but we have already installed " +
                        "the add-on for host [clientKey: {}, baseUrl: {}]. Subsequent installation requests must " +
                        "include valid JWT. Returning 401.", lifecycleEvent.clientKey, lifecycleEvent.baseUrl);
                return responseForMissingJwt();
            }
        } else {
            assertJwtQshClaimPresent(authentication);
            assertHostAuthorized(lifecycleEvent, hostUser);
        }

        AtlassianHost host = maybeExistingHost.orElse(new AtlassianHost());
        updateHostWithLifecycleEvent(host, lifecycleEvent);
        AtlassianHost savedHost = hostRepository.save(host);

        Optional<String> optionalInstallationId = Optional.ofNullable(lifecycleEvent.installationId);
        if (optionalInstallationId.isPresent()) {
            AtlassianHostMapping clientKeyMapping = hostMappingRepository.findById(optionalInstallationId.get()).orElse(new AtlassianHostMapping());
            clientKeyMapping.setInstallationId(optionalInstallationId.get());
            clientKeyMapping.setClientKey(lifecycleEvent.clientKey);
            AtlassianHostMapping savedMapping = hostMappingRepository.save(clientKeyMapping);
            log.info("Saved installation key mapping for host {} ({}, {})", savedHost.getBaseUrl(), savedMapping.getClientKey(), savedMapping.getInstallationId());
        } else {
            long deleteCount = hostMappingRepository.deleteByClientKey(lifecycleEvent.clientKey);
            if (deleteCount > 0) {
                log.info("Removed old installation key mappings for host {} ({})", savedHost.getBaseUrl(), lifecycleEvent.clientKey);
            }
        }

        log.info("Saved installation for host {} ({}, {}, {})", savedHost.getBaseUrl(), savedHost.getClientKey(), savedHost.getCloudId(), savedHost.getAuthentication());

        eventPublisher.publishEventAsynchronously(new AddonInstalledEvent(this, savedHost));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Void> uninstalled(HttpServletRequest request, @Valid @RequestBody LifecycleEvent lifecycleEvent) {
        assertExpectedEventType(lifecycleEvent, "uninstalled");
        lifecycleAuthenticator.setAuthentication(request, lifecycleEvent.baseUrl);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertJwtQshClaimPresent(authentication);

        AtlassianHostUser hostUser = (AtlassianHostUser) authentication.getPrincipal();
        Optional<AtlassianHost> maybeExistingHost = getHostFromLifecycleEvent(lifecycleEvent);
        if (hostUser == null) {
            if (maybeExistingHost.isPresent()) {
                log.error("Uninstallation request was not properly authenticated, but we have already installed " +
                        "the add-on for host [clientKey: {}, baseUrl: {}]. Uninstallation requests must " +
                        "include valid JWT. Returning 401.", lifecycleEvent.clientKey, lifecycleEvent.baseUrl);
                return responseForMissingJwt();
            }
        } else {
            assertHostAuthorized(lifecycleEvent, hostUser);
        }

        if (maybeExistingHost.isPresent()) {
            AtlassianHost host = maybeExistingHost.get();
            host.setAddonInstalled(false);
            hostRepository.save(host);
            log.info("Saved uninstallation for host {} ({})", host.getBaseUrl(), host.getClientKey());
            eventPublisher.publishEventAsynchronously(new AddonUninstalledEvent(this, host));
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private Optional<AtlassianHost> getHostFromLifecycleEvent(LifecycleEvent lifecycleEvent) {
        return hostRepository.findById(lifecycleEvent.clientKey);
    }

    private void updateHostWithLifecycleEvent(AtlassianHost host, LifecycleEvent lifecycleEvent) {
        Optional<String> authenticationType = Optional.ofNullable(lifecycleEvent.authentication);

        Optional<String> installationId = Optional.ofNullable(lifecycleEvent.installationId);
        if (authenticationType.isPresent()) {
            if (LifecycleEvent.CLIENT_CREDENTIALS.equalsIgnoreCase(authenticationType.get())) {
                host.setCloudId(lifecycleEvent.cloudId);
            } else {
                host.setAuthentication(AddonAuthenticationType.JWT);
            }
        } else {
            host.setAuthentication(AddonAuthenticationType.JWT);
        }

        host.setOauthClientId(lifecycleEvent.oauthClientId);
        host.setSharedSecret(lifecycleEvent.sharedSecret);
        host.setClientKey(lifecycleEvent.clientKey);
        host.setBaseUrl(lifecycleEvent.baseUrl);
        host.setDisplayUrl(lifecycleEvent.displayUrl);
        host.setDisplayUrlServicedeskHelpCenter(lifecycleEvent.displayUrlServicedeskHelpCenter);
        host.setProductType(lifecycleEvent.productType);
        host.setDescription(lifecycleEvent.description);
        host.setServiceEntitlementNumber(lifecycleEvent.serviceEntitlementNumber);
        host.setEntitlementId(lifecycleEvent.entitlementId);
        host.setEntitlementNumber(lifecycleEvent.entitlementNumber);
        host.setCapabilitySet(lifecycleEvent.capabilitySet);
        host.setInstallationId(installationId.orElse(null));
        host.setAddonInstalled(true);
    }

    private void assertExpectedEventType(LifecycleEvent lifecycleEvent, String expectedEventType) {
        String eventType = lifecycleEvent.eventType;
        if (!expectedEventType.equals(eventType)) {
            log.error("Received lifecycle callback with unexpected event type {}, expected {}", eventType, expectedEventType);
            throw new InvalidLifecycleEventTypeException();
        }
    }

    private void assertHostAuthorized(LifecycleEvent lifecycleEvent, AtlassianHostUser hostUser) {
        if (!hostUser.getHost().getClientKey().equals(lifecycleEvent.clientKey)) {
            log.error("Installation request was authenticated for host {}, but the host in the body of the request is {}. Returning 403.", hostUser.getHost().getClientKey(), lifecycleEvent.clientKey);
            throw new HostForbiddenException();
        }
    }

    private void assertJwtQshClaimPresent(Authentication authentication) {
        final Optional<AbstractAuthentication> asymmetricAuthentication = Optional.ofNullable(authentication)
                .filter(AbstractAuthentication.class::isInstance)
                .map(AbstractAuthentication.class::cast);
        Optional<JWTClaimsSet> optionalClaimsSet = asymmetricAuthentication
                .map(AbstractAuthentication::getCredentials)
                .filter(JWTClaimsSet.class::isInstance)
                .map(JWTClaimsSet.class::cast);
        if (optionalClaimsSet.isPresent() && !optionalClaimsSet.get().getClaims().containsKey(QUERY_STRING_HASH_CLAIM_NAME)) {
            throw new InvalidJwtTokenTypeException();
        }

        if (asymmetricAuthentication.isPresent()) {
            try {
                asymmetricAuthentication.get().validateQueryStringHash(true);
            } catch (JwtInvalidClaimException e) {
                throw new QshMismatchException(e);
            }
        }
    }

    private ResponseEntity<Void> responseForMissingJwt() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.WWW_AUTHENTICATE, String.format("JWT realm=\"%s\"", addonDescriptorLoader.getDescriptor().getKey()));
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).headers(headers).build();
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid lifecycle event type")
    private static class InvalidLifecycleEventTypeException extends RuntimeException {
    }

    @ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Invalid JWT token type")
    private static class InvalidJwtTokenTypeException extends RuntimeException {
    }

    @ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Qsh mismatch")
    private static class QshMismatchException extends RuntimeException {
        public QshMismatchException(Throwable cause) {
            super(cause);
        }
    }

    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    private static class HostForbiddenException extends RuntimeException {
    }
}
