package com.atlassian.connect.spring.internal.auth.frc;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ForgeApiContext;
import com.atlassian.connect.spring.ForgeContextRetriever;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * A helper class for obtaining the current authenticated {@link AtlassianHostUser}.
 */
@Component
public class ForgeSecurityContextRetriever implements ForgeContextRetriever {

    public Optional<ForgeApiContext> getForgeApiContext() {
        return getForgeApiContextFromAuthentication(SecurityContextHolder.getContext().getAuthentication());
    }

    private Optional<ForgeApiContext> getForgeApiContextFromAuthentication(Authentication authentication) {
        return Optional.ofNullable(authentication)
                .map(Authentication::getDetails)
                .filter(ForgeApiContext.class::isInstance)
                .map(ForgeApiContext.class::cast);
    }
}
