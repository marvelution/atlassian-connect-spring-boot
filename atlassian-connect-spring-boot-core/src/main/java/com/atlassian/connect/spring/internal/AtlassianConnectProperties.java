package com.atlassian.connect.spring.internal;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Properties for Atlassian Connect add-ons.
 */
@ConfigurationProperties(prefix = "atlassian.connect")
public class AtlassianConnectProperties {

    /**
     * The default order for the JWT authentication filter
     */
    public static final int DEFAULT_JWT_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER + 200;

    /**
     * The default order for the Forge Remote authentication filter
     */
    public static final int DEFAULT_FORGE_REMOTE_CONTEXT_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER + 210;

    /**
     * The default order for the Asymmetric authentication filter
     */
    public static final int DEFAULT_ASYMMETRIC_AUTH_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER + 190;

    public static final String DEFAULT_ALL_JS_URL = "https://connect-cdn.atl-paas.net/all.js";

    public static final String DEFAULT_ALL_DEBUG_JS_URL = "https://connect-cdn.atl-paas.net/all-debug.js";
    private String forgeInvocationTokenJwksUrl = "https://forge.cdn.prod.atlassian-dev.net/.well-known/jwks.json";

    public enum RequestUriEncoding {
        /**
         * Let Spring handles the URI encoding
         */
        DEFAULT,

        /**
         * Pass in a completely encoded URI and skip Spring's uri encoding
         */
        NONE,
    }

    /**
     * Allow choosing preferred URI encoding method
     *
     * <p> There have been issues around URIs not being parsed correctly or as expected,
     * see: <a href="https://ecosystem.atlassian.net/browse/ACSPRING-150">ACSPRING-150: RestTemplate not being able to
     * encode URLs with special characters</a>
     *
     * <p> To address this, "none" can be used as your preferred URI encoding method and this allows the URI passed in
     * being untouched - when utilising {@code UriComponentsBuilder} along with {@code build(true)}.
     * <blockquote><pre><code>
     * UriComponents uriComponents = UriComponentsBuilder.fromUriString(preEncodedUriString).build(true); <br>
     * atlassianHostRestClients.authenticatedAsAddon().getForObject(uriComponents.toUri(), Example.class);
     * </code></pre></blockquote>
     *
     * If "none" is not set, it will default to the default behaviour where the URI of URI string passed in will be
     * encoded, and the result may not be desired.
     */
    private RequestUriEncoding uriEncoding = RequestUriEncoding.DEFAULT;

    public RequestUriEncoding getUriEncoding() {
        return uriEncoding;
    }

    public void setUriEncoding(RequestUriEncoding uriEncoding) {
        this.uriEncoding = uriEncoding;
    }

    /**
     * Accept installations signed by an unknown host. (Useful in development mode using an in-memory database.)
     */
    private boolean allowReinstallMissingHost = false;

    /**
     * Enable debug mode for the JavaScript API, loading all-debug.js instead of all.js.
     */
    private boolean debugAllJs = false;

    /**
     * URL of 'all.js' to be returned in Atlassian Connect iframe context parameters
     */
    private String allJsUrl;

    /**
     * The order of the JWT authentication filter in the chain.
     */
    private Integer jwtFilterOrder = DEFAULT_JWT_FILTER_ORDER;

    /**
     * The order of the Forge Remote authentication filter in the chain.
     */
    private Integer forgeFilterOrder = DEFAULT_FORGE_REMOTE_CONTEXT_FILTER_ORDER;

    /**
     * Base URL to retrieve public keys used to verify signed install hooks
     */
    private String publicKeyBaseUrl;

    /**
     * The order of the asymmetric JWT authentication filter in the chain.
     */
    private Integer asymmetricAuthFilterOrder = DEFAULT_ASYMMETRIC_AUTH_FILTER_ORDER;

    public String getPublicKeyBaseUrl() {
        return publicKeyBaseUrl;
    }

    public void setPublicKeyBaseUrl(String publicKeyBaseUrl) {
        this.publicKeyBaseUrl = publicKeyBaseUrl;
    }

    public Integer getAsymmetricAuthFilterOrder() {
        return asymmetricAuthFilterOrder;
    }

    public void setAsymmetricAuthFilterOrder(Integer asymmetricAuthFilterOrder) {
        this.asymmetricAuthFilterOrder = asymmetricAuthFilterOrder;
    }

    /**
     * Expiration time for JSON Web Tokens in seconds.
     */
    private Integer jwtExpirationTime = 180;

    /**
     * Expiration time for self-authentication tokens in seconds.
     */
    private Integer selfAuthenticationExpirationTime = 900;

    /**
     * URL path patterns for which authentication should be required.
     */
    private List<String> requireAuthIncludePaths = new ArrayList<>();

    /**
     * URL path patterns for which authentication should not be required.
     */
    private List<String> requireAuthExcludePaths = new ArrayList<>();

    /**
     * Additional allowed baseUrls that can be used to verify the `aud` claim in the /installed lifecycle callback
     */
    private List<String> allowedBaseUrls = new ArrayList<>();

    /**
     * Request timeout for /installed lifecycle callback in seconds.
     * (If raising above the default, be sure to stay below the host's timeout value.)
     */
    private Integer installTimeout = 3;

    /**
     * Sets up a default redirection for the root of the application to the add-on descriptor resource.
     * Can be disabled if you need to serve e.g a "welcome page" at the root of your application.
     */
    private boolean redirectRootToDescriptor = true;

    public boolean isAllowReinstallMissingHost() {
        return allowReinstallMissingHost;
    }

    public void setAllowReinstallMissingHost(boolean allowReinstallMissingHost) {
        this.allowReinstallMissingHost = allowReinstallMissingHost;
    }

    public boolean isDebugAllJs() {
        return debugAllJs;
    }

    public void setDebugAllJs(boolean debugAllJs) {
        this.debugAllJs = debugAllJs;
    }

    public String getAllJsUrl() {
        if (allJsUrl != null) {
            return allJsUrl;
        }

        return isDebugAllJs() ? DEFAULT_ALL_DEBUG_JS_URL : DEFAULT_ALL_JS_URL;
    }

    public void setAllJsUrl(String allJsUrl) {
        this.allJsUrl = allJsUrl;
    }

    public Integer getJwtFilterOrder() {
        return jwtFilterOrder;
    }

    public void setJwtFilterOrder(Integer jwtFilterOrder) {
        this.jwtFilterOrder = jwtFilterOrder;
    }


    public Integer getForgeFilterOrder() {
        return forgeFilterOrder;
    }

    public void setForgeFilterOrder(Integer forgeFilterOrder) {
        this.forgeFilterOrder = forgeFilterOrder;
    }


    public Integer getJwtExpirationTime() {
        return jwtExpirationTime;
    }

    public void setJwtExpirationTime(Integer jwtExpirationTime) {
        this.jwtExpirationTime = jwtExpirationTime;
    }

    public Integer getSelfAuthenticationExpirationTime() {
        return selfAuthenticationExpirationTime;
    }

    public void setSelfAuthenticationExpirationTime(Integer selfAuthenticationExpirationTime) {
        this.selfAuthenticationExpirationTime = selfAuthenticationExpirationTime;
    }

    public List<String> getRequireAuthIncludePaths() {
        return requireAuthIncludePaths;
    }

    public void setRequireAuthIncludePaths(List<String> requireAuthIncludePaths) {
        this.requireAuthIncludePaths = requireAuthIncludePaths;
    }

    public List<String> getRequireAuthExcludePaths() {
        return requireAuthExcludePaths;
    }

    public void setRequireAuthExcludePaths(List<String> requireAuthExcludePaths) {
        this.requireAuthExcludePaths = requireAuthExcludePaths;
    }

    public List<String> getAllowedBaseUrls() {
        return allowedBaseUrls;
    }

    public void setAllowedBaseUrls(List<String> allowedBaseUrls) {
        this.allowedBaseUrls = allowedBaseUrls;
    }

    public Integer getInstallTimeout() {
        return installTimeout;
    }

    public void setInstallTimeout(Integer installTimeout) {
        this.installTimeout = installTimeout;
    }

    public boolean isRedirectRootToDescriptor() {
        return redirectRootToDescriptor;
    }

    public void setRedirectRootToDescriptor(boolean redirectRootToDescriptor) {
        this.redirectRootToDescriptor = redirectRootToDescriptor;
    }

    public String getForgeInvocationTokenJwksUrl() {
        return forgeInvocationTokenJwksUrl;
    }

    public void setForgeInvocationTokenJwksUrl(String forgeInvocationTokenJwksUrl) {
        this.forgeInvocationTokenJwksUrl = forgeInvocationTokenJwksUrl;
    }
}
