package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;

/**
 * A controller utility class that maps the standard Atlassian Connect iframe context parameters to Spring model
 * attributes.
 */
@ControllerAdvice
public class AtlassianConnectContextModelAttributeProvider {

    private final AtlassianConnectProperties atlassianConnectProperties;

    private final AtlassianConnectSecurityContextHelper securityContextHelper;

    private final SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator;

    public AtlassianConnectContextModelAttributeProvider(AtlassianConnectProperties atlassianConnectProperties,
                                                         AtlassianConnectSecurityContextHelper securityContextHelper,
                                                         SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator) {
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.securityContextHelper = securityContextHelper;
        this.selfAuthenticationTokenGenerator = selfAuthenticationTokenGenerator;
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("atlassianConnectLicense", getLicense());
        model.addAttribute("atlassianConnectAllJsUrl", getAllJsUrl());
        model.addAttribute("atlassianConnectToken", getSelfAuthenticationToken());
    }

    public String getLicense() {
        return Optional.ofNullable(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()))
                .map(attributes -> attributes.getRequest().getParameter("lic"))
                .orElse("");
    }

    public String getAllJsUrl() {
        return atlassianConnectProperties.getAllJsUrl();
    }

    public String getSelfAuthenticationToken() {
        return securityContextHelper.getHostUserFromSecurityContext()
                .map(selfAuthenticationTokenGenerator::createSelfAuthenticationToken)
                .orElse(null);
    }
}
