package com.atlassian.connect.spring.internal.lifecycle;

import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

/**
 * A utility that executes transactions with optional rollback.
 */
@Service
public class TransactionExecutor {

    private static final Logger log = LoggerFactory.getLogger(TransactionExecutor.class);

    private final Optional<TransactionTemplate> optionalTransactionTemplate;

    public TransactionExecutor(Optional<TransactionTemplate> optionalTransactionTemplate) {
        this.optionalTransactionTemplate = optionalTransactionTemplate;
    }

    /**
     * Executes the given supplier in a transaction, allowing the caller to trigger rollback before its completion
     * by setting the provided flag.
     *
     * @param supplier       the function to execute
     * @param shouldRollback a flag indicating whether the transaction should be rolled back once the function has been executed
     * @param <T>            the return type of the function
     * @return the value returned by the supplier
     */
    @Async
    public <T> CompletableFuture<T> executeWithRollbackOption(Supplier<T> supplier, AtomicBoolean shouldRollback) {
        TransactionCallback<T> callback = new TransactionCallbackWithTimeout<>(supplier, shouldRollback);
        T result = optionalTransactionTemplate.map(template -> template.execute(callback)).orElseGet(supplier);
        return CompletableFuture.completedFuture(result);
    }

    private record TransactionCallbackWithTimeout<T>(Supplier<T> supplier,
                                                     AtomicBoolean shouldRollback) implements TransactionCallback<T> {

        @Override
        public T doInTransaction(@Nonnull TransactionStatus status) {
            T result = supplier.get();
            if (shouldRollback.get()) {
                log.debug("Rolling back transaction.");
                status.setRollbackOnly();
            }
            return result;
        }
    }
}
