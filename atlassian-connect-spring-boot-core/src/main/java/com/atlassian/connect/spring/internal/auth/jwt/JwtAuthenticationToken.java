package com.atlassian.connect.spring.internal.auth.jwt;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collections;
import java.util.Objects;

/**
 * An authentication token for a set of unverified {@link JwtCredentials JSON Web Token credentials}.
 *
 * @see JwtAuthentication
 * @see JwtCredentials
 */
public class JwtAuthenticationToken extends AbstractAuthenticationToken {

    private final JwtCredentials jwtCredentials;

    public JwtAuthenticationToken(JwtCredentials jwtCredentials) {
        super(Collections.emptySet());
        this.jwtCredentials = jwtCredentials;
    }

    @Override
    public JwtCredentials getCredentials() {
        return jwtCredentials;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        JwtAuthenticationToken that = (JwtAuthenticationToken) o;
        return Objects.equals(jwtCredentials, that.jwtCredentials);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), jwtCredentials);
    }
}
