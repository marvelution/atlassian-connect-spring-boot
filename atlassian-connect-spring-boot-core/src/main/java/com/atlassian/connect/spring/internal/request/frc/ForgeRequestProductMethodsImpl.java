package com.atlassian.connect.spring.internal.request.frc;

import com.atlassian.connect.spring.ForgeRequestProductMethods;
import org.springframework.web.client.RestTemplate;

public class ForgeRequestProductMethodsImpl implements ForgeRequestProductMethods {
    private final ForgeRemoteRequestAuthType providerType;
    private final ForgeRestTemplateFactory forgeRestTemplateFactory;

    public ForgeRequestProductMethodsImpl(ForgeRemoteRequestAuthType providerType,
                                          ForgeRestTemplateFactory forgeRestTemplateFactory) {
        this.providerType = providerType;
        this.forgeRestTemplateFactory = forgeRestTemplateFactory;
    }

    @Override
    public RestTemplate request() {
        return forgeRestTemplateFactory.getForgeRestTemplate(this.providerType, ForgeRemoteRequestType.OTHER);
    }

    @Override
    public RestTemplate requestConfluence() {
        return forgeRestTemplateFactory.getForgeRestTemplate(this.providerType, ForgeRemoteRequestType.CONFLUENCE);
    }

    @Override
    public RestTemplate requestJira() {
        return forgeRestTemplateFactory.getForgeRestTemplate(this.providerType, ForgeRemoteRequestType.JIRA);
    }

}
