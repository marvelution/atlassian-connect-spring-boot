package com.atlassian.connect.spring.internal.request.frc;

public enum ForgeRemoteRequestType {
    JIRA,
    CONFLUENCE,
    OTHER
}
