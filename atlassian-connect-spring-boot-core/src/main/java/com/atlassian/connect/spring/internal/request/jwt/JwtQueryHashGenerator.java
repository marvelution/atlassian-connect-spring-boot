package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.security.NoSuchAlgorithmException;

@Component
public class JwtQueryHashGenerator {

    public CanonicalHttpRequest createCanonicalHttpRequest(HttpMethod httpMethod, URI uri, String baseUrl) {
        final String hostContextPath = URI.create(baseUrl).getPath();
        UriComponents uriComponents = UriComponentsBuilder.fromUri(uri).build();
        return new CanonicalHttpUriComponentsRequest(httpMethod, uriComponents, hostContextPath);
    }

    public String computeCanonicalRequestHash(CanonicalHttpRequest canonicalHttpRequest) {
        try {
            return HttpRequestCanonicalizer.computeCanonicalRequestHash(canonicalHttpRequest);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
