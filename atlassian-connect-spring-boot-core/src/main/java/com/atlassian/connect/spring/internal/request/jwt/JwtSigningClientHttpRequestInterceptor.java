package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import com.atlassian.connect.spring.internal.request.UserAgentProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

/**
 * A {@link ClientHttpRequestInterceptor} that signs requests to Atlassian hosts with JSON Web Tokens.
 */
public class JwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private final JwtGenerator jwtGenerator;

    private final AtlassianHostUriResolver hostUriResolver;

    private final Function<HttpRequest, Optional<AtlassianHost>> hostSupplier;

    public JwtSigningClientHttpRequestInterceptor(
            JwtGenerator jwtGenerator,
            AtlassianHostUriResolver hostUriResolver,
            AtlassianConnectSecurityContextHelper securityContextHelper,
            UserAgentProvider userAgentProvider) {
        super(userAgentProvider::getUserAgent);
        this.jwtGenerator = jwtGenerator;
        this.hostUriResolver = hostUriResolver;
        this.hostSupplier = request -> getHostFromContext(this.hostUriResolver, securityContextHelper, request);
    }

    public JwtSigningClientHttpRequestInterceptor(
            JwtGenerator jwtGenerator,
            AtlassianHostUriResolver hostUriResolver,
            AtlassianHost host,
            UserAgentProvider userAgentProvider) {
        super(userAgentProvider::getUserAgent);
        this.jwtGenerator = jwtGenerator;
        this.hostUriResolver = hostUriResolver;
        this.hostSupplier = request -> Optional.of(host);
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        return hostSupplier.apply(request);
    }

    @Override
    protected URI wrapUri(HttpRequest request, AtlassianHost host) {
        URI uri = request.getURI();
        return uri.isAbsolute() ? uri : this.hostUriResolver.resolveToAbsoluteUriWithBase(uri, host.getBaseUrl());
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        String jwt = jwtGenerator.createJwtToken(request.getMethod(), request.getURI(), host);
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", jwt));
        return request;
    }

    private Optional<AtlassianHost> getHostFromContext(AtlassianHostUriResolver hostUriResolver, AtlassianConnectSecurityContextHelper securityContextHelper, HttpRequest request) {
        Optional<AtlassianHost> optionalHost = securityContextHelper.getHostFromSecurityContext()
                .filter(host -> AtlassianHostUriResolver.isRequestToHost(request.getURI(), host));
        if (optionalHost.isEmpty()) {
            optionalHost = hostUriResolver.getHostFromRequestUrl(request.getURI());
        }
        return optionalHost;
    }
}
