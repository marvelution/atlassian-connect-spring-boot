package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.LifecycleURLHelper;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpServletRequest;
import jakarta.annotation.Nonnull;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

/**
 * A servlet filter that extracts JSON Web Tokens from the Authorization request header and from the <code>jwt</code>
 * query parameter for use as authentication tokens.
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String AUTHORIZATION_HEADER_SCHEME_PREFIX = "JWT ";

    private static final String QUERY_PARAMETER_NAME = "jwt";

    private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    private final AuthenticationManager authenticationManager;

    private final AtlassianConnectProperties atlassianConnectProperties;

    private final AuthenticationFailureHandler failureHandler;

    private final LifecycleURLHelper lifecycleURLHelper;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,
                                   AtlassianConnectProperties atlassianConnectProperties,
                                   ServerProperties serverProperties, LifecycleURLHelper lifecycleURLHelper) {
        this.authenticationManager = authenticationManager;
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.failureHandler = createFailureHandler(serverProperties);
        this.lifecycleURLHelper = lifecycleURLHelper;
    }

    @Override
    protected void doFilterInternal(@Nonnull HttpServletRequest request,
                                    @Nonnull HttpServletResponse response,
                                    @Nonnull FilterChain filterChain) throws ServletException, IOException {
        Optional<String> optionalJwt = getJwtFromRequest(request);

        if (lifecycleURLHelper.isRequestToLifecycleURL(request)) {
            log.debug("Ignoring lifecycle endpoint");
            filterChain.doFilter(request, response);
            return;
        }

        if (optionalJwt.isPresent()) {
            Authentication authenticationRequest = createJwtAuthenticationToken(request, optionalJwt.get());

            Authentication authenticationResult;
            try {
                authenticationResult = authenticationManager.authenticate(authenticationRequest);
                SecurityContextHolder.getContext().setAuthentication(authenticationResult);
            } catch (ProviderNotFoundException e) {
                log.info("Could not authenticate the request with symmetric signature");
            } catch (AuthenticationException e) {
                log.warn("Failed to authenticate request", e);
                if (shouldIgnoreInvalidJwt(request, e)) {
                    log.warn("Received JWT authentication from unknown host ({}), but allowing anyway",
                            ((UnknownJwtIssuerException) e).getIssuer());
                } else {
                    failureHandler.onAuthenticationFailure(request, response, e);
                    return;
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    private SimpleUrlAuthenticationFailureHandler createFailureHandler(ServerProperties serverProperties) {
        SimpleUrlAuthenticationFailureHandler handler = new SimpleUrlAuthenticationFailureHandler(serverProperties.getError().getPath());
        handler.setAllowSessionCreation(false);
        handler.setUseForward(true);
        return handler;
    }

    private static Optional<String> getJwtFromRequest(HttpServletRequest request) {
        Optional<String> optionalJwt = getJwtFromHeader(request);
        if (optionalJwt.isEmpty()) {
            optionalJwt = getJwtFromParameter(request);
        }
        return optionalJwt;
    }

    private static Optional<String> getJwtFromHeader(HttpServletRequest request) {
        Optional<String> optionalJwt = Optional.empty();
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (!ObjectUtils.isEmpty(authHeader) && authHeader.startsWith(AUTHORIZATION_HEADER_SCHEME_PREFIX)) {
            String jwt = authHeader.substring(AUTHORIZATION_HEADER_SCHEME_PREFIX.length());
            optionalJwt = Optional.of(jwt);
        }

        return optionalJwt;
    }

    private static Optional<String> getJwtFromParameter(HttpServletRequest request) {
        Optional<String> optionalJwt = Optional.empty();
        String jwt = request.getParameter(QUERY_PARAMETER_NAME);
        if (!ObjectUtils.isEmpty(jwt)) {
            optionalJwt = Optional.of(jwt);
        }
        return optionalJwt;
    }

    private JwtAuthenticationToken createJwtAuthenticationToken(HttpServletRequest request, String jwt) {
        log.debug("Retrieved JWT from request");
        CanonicalHttpServletRequest canonicalHttpServletRequest = new CanonicalHttpServletRequest(request);
        JwtCredentials credentials = new JwtCredentials(jwt, canonicalHttpServletRequest);
        return new JwtAuthenticationToken(credentials);
    }

    private boolean shouldIgnoreInvalidJwt(HttpServletRequest request, AuthenticationException e) {
        return e instanceof UnknownJwtIssuerException
                && ((lifecycleURLHelper.isRequestToInstalledLifecycle(request) && atlassianConnectProperties.isAllowReinstallMissingHost())
                || lifecycleURLHelper.isRequestToUninstalledLifecycle(request));
    }

}
