package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import com.atlassian.connect.spring.internal.request.UserAgentProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * A provider of a {@link RestTemplate} that signs requests to Atlassian hosts with JSON Web Tokens.
 */
@Component
public class JwtSigningRestTemplateFactory {

    private final RestTemplateBuilder restTemplateBuilder;
    private final JwtGenerator jwtGenerator;
    private final UserAgentProvider userAgentProvider;
    private final RestTemplate restTemplate;

    private final AtlassianHostUriResolver hostUriResolver;

    @Autowired
    public JwtSigningRestTemplateFactory(RestTemplateBuilder restTemplateBuilder,
                                         JwtGenerator jwtGenerator,
                                         AtlassianHostUriResolver hostUriResolver,
                                         AtlassianConnectSecurityContextHelper securityContextHelper,
                                         UserAgentProvider userAgentProvider) {
        this.restTemplateBuilder = restTemplateBuilder;
        this.jwtGenerator = jwtGenerator;
        this.userAgentProvider = userAgentProvider;
        this.hostUriResolver = hostUriResolver;

        JwtSigningClientHttpRequestInterceptor requestInterceptor = new JwtSigningClientHttpRequestInterceptor(
                jwtGenerator, hostUriResolver, securityContextHelper, userAgentProvider);
        restTemplate = restTemplateBuilder.additionalInterceptors(requestInterceptor).build();
    }

    public RestTemplate getJwtRestTemplate() {
        return restTemplate;
    }

    public RestTemplate getJwtRestTemplate(AtlassianHost host) {
        JwtSigningClientHttpRequestInterceptor requestInterceptor = new JwtSigningClientHttpRequestInterceptor(
                jwtGenerator, hostUriResolver, host, userAgentProvider);
        return restTemplateBuilder.additionalInterceptors(requestInterceptor).build();
    }
}
