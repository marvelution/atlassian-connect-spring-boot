package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import com.atlassian.connect.spring.internal.request.UserAgentProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

@Component
public class OAuth2RestTemplateFactory {

    private final RestTemplateBuilder restTemplateBuilder;
    private final OAuth2JwtTokenService oAuth2JwtTokenService;
    private final UserAgentProvider userAgentProvider;
    private final AtlassianHostUriResolver atlassianHostUriResolver;

    @Autowired
    public OAuth2RestTemplateFactory(
            RestTemplateBuilder restTemplateBuilder,
            OAuth2JwtTokenService oAuth2JwtTokenService,
            UserAgentProvider userAgentProvider,
            AtlassianHostUriResolver atlassianHostUriResolver) {
        this.restTemplateBuilder = restTemplateBuilder;
        this.oAuth2JwtTokenService = oAuth2JwtTokenService;
        this.userAgentProvider = userAgentProvider;
        this.atlassianHostUriResolver = atlassianHostUriResolver;
    }

    @Cacheable(value = {"oauth-2-clients"})
    public RestTemplate getOAuth2RestTemplate(AtlassianHostUser hostUser) {
        assertValidHostUser(hostUser);
        return restTemplateBuilder.additionalInterceptors(new OAuth2HttpRequestInterceptor(
                hostUser, oAuth2JwtTokenService, userAgentProvider, atlassianHostUriResolver)).build();
    }

    private void assertValidHostUser(AtlassianHostUser hostUser) {
        if (ObjectUtils.isEmpty(hostUser.getHost().getOauthClientId())) {
            throw new IllegalArgumentException("Can not act as a user for a host with no OAuthClientId. " +
                    "Make sure you have ACT_AS_USER scope specified in your descriptor.");
        }

        if (hostUser.getUserAccountId().isEmpty()) {
            throw new IllegalArgumentException("The provided AtlassianHostUser did not specify a user to act as.");
        }
    }

}
