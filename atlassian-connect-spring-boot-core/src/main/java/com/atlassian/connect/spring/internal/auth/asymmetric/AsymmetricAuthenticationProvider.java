package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.AbstractConnectAuthenticationProvider;
import com.atlassian.connect.spring.internal.auth.jwt.InvalidJwtException;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.AbstractJwtReader;
import com.atlassian.connect.spring.internal.jwt.JwtInvalidSigningAlgorithmException;
import com.atlassian.connect.spring.internal.jwt.JwtParseException;
import com.atlassian.connect.spring.internal.jwt.JwtVerificationException;
import com.atlassian.connect.spring.internal.jwt.RsaJwtReader;
import com.nimbusds.jwt.JWTClaimsSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class AsymmetricAuthenticationProvider extends AbstractConnectAuthenticationProvider {

    private static final Logger log = LoggerFactory.getLogger(AsymmetricAuthenticationProvider.class);
    private final UnaryOperator<String> publicKeyBaseURISupplier;
    private final AsymmetricPublicKeyProvider asymmetricPublicKeyProvider;
    private final AtlassianConnectProperties atlassianConnectProperties;
    private final List<URI> allowedBaseUrlsFromConfig;

    public AsymmetricAuthenticationProvider(AddonDescriptorLoader addonDescriptorLoader,
                                            AtlassianHostRepository hostRepository,
                                            UnaryOperator<String> publicKeyBaseURISupplier,
                                            AsymmetricPublicKeyProvider asymmetricPublicKeyProvider,
                                            AtlassianConnectProperties atlassianConnectProperties) {
        super(addonDescriptorLoader, hostRepository);
        this.publicKeyBaseURISupplier = publicKeyBaseURISupplier;
        this.asymmetricPublicKeyProvider = asymmetricPublicKeyProvider;
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.allowedBaseUrlsFromConfig = new ArrayList<>(atlassianConnectProperties.getAllowedBaseUrls().stream().map(URI::create).toList());
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        AsymmetricAuthenticationToken pkiAuthentication = (AsymmetricAuthenticationToken) authentication;
        AsymmetricCredentials credentials = pkiAuthentication.getCredentials();

        try {
            String tenantBaseUrl = pkiAuthentication.getTenantBaseUrl();
            String keyId = RsaJwtReader.getKeyIdAndCheckSigningAlgorithm(credentials.getRawJwt());
            String publicKey = asymmetricPublicKeyProvider.fetchPublicKey(publicKeyBaseURISupplier.apply(tenantBaseUrl), keyId);

            AbstractJwtReader reader = new RsaJwtReader(publicKey);
            JWTClaimsSet claims = reader.readAndVerify(credentials.getRawJwt(), null);

            return buildAsymmetricAuthentication(credentials, claims);
        } catch (JwtInvalidSigningAlgorithmException e) {
            String message = "Unexpected JWT signing algorithm, most likely due to install request signed with a shared secret";
            log.info(message, e);
            throw new InvalidJwtException(message, e);
        } catch (JwtVerificationException | JwtParseException e) {
            String message = "Could not verify incoming JWT";
            log.warn(message, e);
            throw new InvalidJwtException(message, e);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            String message = "Could initialize RSA verifier";
            log.error(message, e);
            throw new AuthenticationServiceException(message, e);
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(AsymmetricAuthenticationToken.class);
    }

    protected Authentication buildAsymmetricAuthentication(AsymmetricCredentials credentials, JWTClaimsSet claims) {
        String audienceClaim = claims.getAudience().stream().findFirst().orElse("");
        List<URI> allowedBaseUrls = Stream.concat(this.allowedBaseUrlsFromConfig.stream(), Stream.of(URI.create(addonDescriptorLoader.getDescriptor().getBaseUrl()))).toList();

        if (allowedBaseUrls.stream().filter(baseUrl -> baseUrl.equals(URI.create(audienceClaim))).findAny().isEmpty()) {
            String message = String.format("Audience claim '%s' in JWT does not match any app baseURL from the allowed list of baseURLs: '%s'",
                    audienceClaim,
                    allowedBaseUrls);
            log.warn(message);
            throw new InvalidJwtException("Wrong audience found in incoming JWT");
        }

        String hostClientKey = claims.getIssuer();
        Optional<AtlassianHost> host = hostRepository.findById(hostClientKey);
        Optional<AtlassianHostUser> hostUser = host.map(h -> createHostUserFromSubjectClaim(h, claims));
        String qsh = this.computeQueryStringHash(credentials);
        return new AsymmetricAuthentication(hostUser.orElse(null), claims, qsh);
    }
}
