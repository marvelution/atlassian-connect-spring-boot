package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.CanonicalRequestUtil;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * A generator of JSON Web Tokens.
 */
@Component
public class JwtGenerator {

    private static final Logger log = LoggerFactory.getLogger(JwtGenerator.class);

    private final AtlassianHostUriResolver hostUriResolver;

    private final AddonDescriptorLoader addonDescriptorLoader;

    private final AtlassianConnectProperties atlassianConnectProperties;

    private final JwtQueryHashGenerator queryHashGenerator;

    @Autowired
    public JwtGenerator(AtlassianHostUriResolver hostUriResolver,
                        AddonDescriptorLoader addonDescriptorLoader,
                        AtlassianConnectProperties atlassianConnectProperties,
                        JwtQueryHashGenerator queryHashGenerator) {
        this.hostUriResolver = hostUriResolver;
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.queryHashGenerator = queryHashGenerator;
    }

    public String createJwtToken(HttpMethod httpMethod, URI uri) {
        assertUriAbsolute(uri);
        return internalCreateJwtToken(httpMethod, uri, getHostFromRequestUri(uri));
    }

    public String createJwtToken(HttpMethod httpMethod, URI uri, AtlassianHost host) {
        assertUriAbsolute(uri);
        assertRequestToHost(uri, host);

        return internalCreateJwtToken(httpMethod, uri, host);
    }

    private String internalCreateJwtToken(HttpMethod httpMethod, URI uri, AtlassianHost host) {
        CanonicalHttpRequest canonicalHttpRequest = queryHashGenerator.createCanonicalHttpRequest(httpMethod, uri, host.getBaseUrl());
        log.debug("Generating JWT with canonical request: {}", CanonicalRequestUtil.toVerboseString(canonicalHttpRequest));
        String queryHash = queryHashGenerator.computeCanonicalRequestHash(canonicalHttpRequest);

        Duration expirationTime = Duration.of(atlassianConnectProperties.getJwtExpirationTime(), ChronoUnit.SECONDS);
        JwtBuilder jwtBuilder = new JwtBuilder(expirationTime)
                .issuer(addonDescriptorLoader.getDescriptor().getKey())
                // .audience(host.getClientKey()) -- TODO Figure out whether we can / should set this?
                .queryHash(queryHash)
                .signature(host.getSharedSecret());
        return jwtBuilder.build();
    }

    private void assertUriAbsolute(URI uri) {
        if (!uri.isAbsolute()) {
            throw new IllegalArgumentException("The given URI is not absolute");
        }
    }

    private void assertRequestToHost(URI uri, AtlassianHost host) {
        if (!AtlassianHostUriResolver.isRequestToHost(uri, host)) {
            throw new IllegalArgumentException("The given URI is not under the base URL of the given host");
        }
    }

    private AtlassianHost getHostFromRequestUri(URI uri) {
        return hostUriResolver.getHostFromRequestUrl(uri).orElseThrow(
                () -> new IllegalArgumentException("The given URI is not under the base URL of any installed host"));
    }
}
