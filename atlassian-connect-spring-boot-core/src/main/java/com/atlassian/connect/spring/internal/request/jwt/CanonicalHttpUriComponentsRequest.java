package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.CanonicalRequestUtil;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;

import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CanonicalHttpUriComponentsRequest implements CanonicalHttpRequest {

    private final String httpMethod;
    private final String relativePath;
    private final Map<String, String[]> parameterMap;

    public CanonicalHttpUriComponentsRequest(HttpMethod httpMethod, UriComponents uriComponents, String contextPath) {
        this.httpMethod = httpMethod.name();
        this.relativePath = CanonicalRequestUtil.getRelativePath(uriComponents.getPath(), contextPath);
        this.parameterMap = toArrayMap(uriComponents.getQueryParams());
    }

    @Override
    public String getMethod() {
        return httpMethod;
    }

    @Override
    public String getRelativePath() {
        return relativePath;
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return parameterMap;
    }

    private static Map<String, String[]> toArrayMap(MultiValueMap<String, String> queryParams) {
        final Map<String, String[]> result = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : queryParams.entrySet()) {
            List<String> values = entry.getValue().stream().map(CanonicalHttpUriComponentsRequest::decode).toList();
            result.put(decode(entry.getKey()), values.toArray(new String[entry.getValue().size()]));
        }
        return result;
    }

    private static String decode(String value) {
        return URLDecoder.decode(value, Charset.defaultCharset());
    }
}
