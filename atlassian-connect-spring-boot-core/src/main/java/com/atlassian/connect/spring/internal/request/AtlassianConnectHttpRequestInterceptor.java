package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHost;
import jakarta.annotation.Nonnull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;
import java.util.function.Supplier;

public abstract class AtlassianConnectHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    private final Supplier<String> userAgentSupplier;

    protected AtlassianConnectHttpRequestInterceptor(Supplier<String> userAgentSupplier) {
        this.userAgentSupplier = userAgentSupplier;
    }

    @Override
    @Nonnull
    public ClientHttpResponse intercept(@Nonnull HttpRequest request,
                                        @Nonnull byte[] body,
                                        @Nonnull ClientHttpRequestExecution execution) throws IOException {
        HttpRequest interceptedRequest = getHostForRequest(request).map(host -> wrapRequest(request, host)).orElse(request);
        return execution.execute(interceptedRequest, body);
    }

    protected abstract Optional<AtlassianHost> getHostForRequest(HttpRequest request);

    protected void assertRequestToHost(HttpRequest request, AtlassianHost host) {
        final URI requestUri = request.getURI();
        if (requestUri.isAbsolute() && !AtlassianHostUriResolver.isRequestToHost(requestUri, host)) {
            throw new IllegalArgumentException("Aborting the request to "
                    + requestUri.toASCIIString() +
                    " because this RestTemplate is associated with " + host.getBaseUrl());
        }
    }

    @SuppressWarnings("unused")
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        return request;
    }

    protected abstract URI wrapUri(HttpRequest request, AtlassianHost host);

    private HttpRequest wrapRequest(HttpRequest request, AtlassianHost host) {
        URI uri = wrapUri(request, host);
        String userAgent = userAgentSupplier.get();
        HttpRequestWrapper requestWrapper = new AtlassianConnectHttpRequestWrapper(request, uri, userAgent);
        return rewrapRequest(requestWrapper, host);
    }

    private static class AtlassianConnectHttpRequestWrapper extends HttpRequestWrapper {

        private final URI uri;

        public AtlassianConnectHttpRequestWrapper(HttpRequest request, URI uri, String userAgent) {
            super(request);
            this.uri = uri;

            super.getHeaders().set(HttpHeaders.USER_AGENT, userAgent);
        }

        @Override
        @Nonnull
        public URI getURI() {
            return uri;
        }
    }
}
