package com.atlassian.connect.spring.internal.auth.asymmetric;

import com.atlassian.connect.spring.internal.auth.jwt.InvalidJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class AsymmetricPublicKeyProvider {
    private static final Logger log = LoggerFactory.getLogger(AsymmetricPublicKeyProvider.class);

    private final RestTemplate restTemplate;

    public AsymmetricPublicKeyProvider(RestTemplateBuilder templateBuilder) {
        this.restTemplate = templateBuilder.build();
    }

    /**
     * Public to enable integration testing.
     *
     * @return the rest template used for access token requests
     */
    public RestTemplate getAsymmetricPublicKeyProviderRestTemplate() {
        return restTemplate;
    }

    public String fetchPublicKey(String publicKeyHost, String keyId) {
        URI keyHostUrl = URI.create(publicKeyHost + "/" + keyId);
        try {
            ResponseEntity<String> response
                    = restTemplate.getForEntity(keyHostUrl.normalize().toString(), String.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                String message = String.format("Could not find public key for keyId '%s'", keyId);
                log.warn(message, e);
                throw new InvalidJwtException(message, e);
            }
            String message = String.format("Unexpect error when fetching public key from '%s'", keyHostUrl.normalize());
            log.error(message, e);
            throw new AuthenticationServiceException(message, e);
        }
    }
}
