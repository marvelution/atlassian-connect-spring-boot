package com.atlassian.connect.spring.internal.request.oauth2;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

@Component
public class OAuth2JwtAccessTokenResponseClient implements OAuth2AccessTokenResponseClient<OAuth2JwtAssertionAuthorizationGrantRequest> {

    private final RestTemplate restTemplate;

    private final OAuth2JwtAssertionGenerator jwtAssertionGenerator;

    public OAuth2JwtAccessTokenResponseClient(RestTemplateBuilder restTemplateBuilder,
                                              OAuth2JwtAssertionGenerator jwtAssertionGenerator) {
        this.restTemplate = restTemplateBuilder
                .messageConverters(new FormHttpMessageConverter(), new OAuth2AccessTokenResponseHttpMessageConverter())
                .errorHandler(new OAuth2ErrorResponseErrorHandler())
                .build();
        this.jwtAssertionGenerator = jwtAssertionGenerator;
    }

    @Override
    public OAuth2AccessTokenResponse getTokenResponse(OAuth2JwtAssertionAuthorizationGrantRequest authorizationGrantRequest) {
        Assert.notNull(authorizationGrantRequest, "clientCredentialsGrantRequest cannot be null");

        ClientRegistration clientRegistration = authorizationGrantRequest.getClientRegistration();
        String tokenUri = clientRegistration.getProviderDetails().getTokenUri();
        String assertionString = jwtAssertionGenerator.getAssertionString(authorizationGrantRequest.getHostUser(), tokenUri);

        final HttpHeaders contentTypeHeader = new HttpHeaders();
        contentTypeHeader.setContentType(APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> stringStringLinkedMultiValueMap = accessTokenRequestParameters(assertionString,
                clientRegistration.getAuthorizationGrantType());
        final HttpEntity<MultiValueMap<String, String>> accessTokenRequestEntity =
                new HttpEntity<>(stringStringLinkedMultiValueMap, contentTypeHeader);

        ResponseEntity<OAuth2AccessTokenResponse> accessTokenResponse = restTemplate.exchange(
                tokenUri, HttpMethod.POST, accessTokenRequestEntity, OAuth2AccessTokenResponse.class);
        return accessTokenResponse.getBody();
    }

    /**
     * Public to enable integration testing.
     *
     * @return the rest template used for access token requests
     */
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    private static MultiValueMap<String, String> accessTokenRequestParameters(String assertion, AuthorizationGrantType grantType) {
        LinkedMultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add(OAuth2ParameterNames.GRANT_TYPE, grantType.getValue());
        parameters.add("assertion", assertion);
        return parameters;
    }
}
