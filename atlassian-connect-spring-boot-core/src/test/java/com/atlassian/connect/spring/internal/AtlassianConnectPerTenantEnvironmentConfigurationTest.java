package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.AtlassianHost;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class AtlassianConnectPerTenantEnvironmentConfigurationTest {
    private static final String TENANT_URL_IN_STG = "https://test.jira-dev.com";
    private static final String TENANT_URL_IN_PROD = "https://test.atlassian.net";
    private static final String TENANT_URL_IN_FEDRAMP_STG = "https://test.atlassian-stg-fedm.net";
    private static final String TENANT_URL_IN_FEDRAMP_PROD = "https://test.atlassian-us-gov-mod.net";
    private static final String AUTHORIZATION_SERVER_HOST_STAGING = "oauth-2-authorization-server.stg.services.atlassian.com";
    private static final String AUTHORIZATION_SERVER_HOST_PRODUCTION = "oauth-2-authorization-server.services.atlassian.com";
    private static final String AUTHORIZATION_SERVER_HOST_FEDRAMP_STAGING = "oauth-2-authorization-server.stg.services.atlassian-us-gov-mod.com";
    private static final String AUTHORIZATION_SERVER_HOST_FEDRAMP_PRODUCTION = "oauth-2-authorization-server.services.atlassian-us-gov-mod.com";
    private static final String KEY_SERVER_URL_STAGING = "https://cs-migrations--cdn.us-east-1.staging.public.atl-paas.net";
    private static final String KEY_SERVER_URL_FEDRAMP_STAGING = "https://cs-migrations--fedrampcdn.us-east-1.staging.cdn.atlassian-us-gov-mod.com";
    private static final String KEY_SERVER_URL_PROD = "https://connect-install-keys.atlassian.com";
    private static final String KEY_SERVER_URL_FEDRAMP_PROD = "https://cs-migrations--fedrampcdn.us-east-1.prod.cdn.atlassian-us-gov-mod.com";
    private static final String TEST_KEY_SERVER_URL = "https://example.com";
    private AtlassianHost host = new AtlassianHost();

    @Mock
    private AtlassianConnectProperties properties;
    @InjectMocks
    private AtlassianConnectPerTenantEnvironmentConfiguration perTenantEnvironmentConfiguration;

    @Test
    void shouldReturnStgOAuth2ServerUrlForStgTenant() {
        host.setBaseUrl(TENANT_URL_IN_STG);
        assertThat(perTenantEnvironmentConfiguration.getAuthorizationServerBaseUrl(host).getHost(), is(AUTHORIZATION_SERVER_HOST_STAGING));
    }

    @Test
    void shouldReturnFedRAMPOAuth2ServerUrlForFedRAMPStaging() {
        host.setBaseUrl(TENANT_URL_IN_FEDRAMP_STG);
        assertThat(perTenantEnvironmentConfiguration.getAuthorizationServerBaseUrl(host).getHost(), is(AUTHORIZATION_SERVER_HOST_FEDRAMP_STAGING));
    }

    @Test
    void shouldReturnProdOAuth2ServerUrlForProdTenant() {
        host.setBaseUrl(TENANT_URL_IN_PROD);
        assertThat(perTenantEnvironmentConfiguration.getAuthorizationServerBaseUrl(host).getHost(), is(AUTHORIZATION_SERVER_HOST_PRODUCTION));
    }
    @Test
    void shouldReturnFedRAMPOAuth2ServerUrlInFedRAMPProd() {
        host.setBaseUrl(TENANT_URL_IN_FEDRAMP_PROD);
        assertThat(perTenantEnvironmentConfiguration.getAuthorizationServerBaseUrl(host).getHost(), is(AUTHORIZATION_SERVER_HOST_FEDRAMP_PRODUCTION));
    }

    @Test
    void shouldReturnStgKeyServerUrlForStgTenant() {
        assertThat(perTenantEnvironmentConfiguration.getPublicKeyBaseUrl(TENANT_URL_IN_STG), is(KEY_SERVER_URL_STAGING));
    }
    @Test
    void shouldReturnFedRAMPStgKeyServerUrlForFedRAMPStgTenant() {
        assertThat(perTenantEnvironmentConfiguration.getPublicKeyBaseUrl(TENANT_URL_IN_FEDRAMP_STG), is(KEY_SERVER_URL_FEDRAMP_STAGING));
    }
    @Test
    void shouldReturnProdKeyServerUrlForProdTenant() {
        assertThat(perTenantEnvironmentConfiguration.getPublicKeyBaseUrl(TENANT_URL_IN_PROD), is(KEY_SERVER_URL_PROD));
    }

    @Test
    void shouldReturnFedRAMPProdKeyServerUrlForFedRAMPProdTenant() {
        assertThat(perTenantEnvironmentConfiguration.getPublicKeyBaseUrl(TENANT_URL_IN_FEDRAMP_PROD), is(KEY_SERVER_URL_FEDRAMP_PROD));
    }

    @Test
    void shouldReturnSetKeyServerUrl() {
        doReturn(TEST_KEY_SERVER_URL).when(properties).getPublicKeyBaseUrl();
        assertThat(perTenantEnvironmentConfiguration.getPublicKeyBaseUrl(TENANT_URL_IN_PROD), is(TEST_KEY_SERVER_URL));
    }
}
