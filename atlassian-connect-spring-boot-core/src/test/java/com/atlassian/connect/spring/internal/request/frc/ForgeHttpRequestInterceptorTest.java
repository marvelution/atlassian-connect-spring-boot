package com.atlassian.connect.spring.internal.request.frc;

import com.atlassian.connect.spring.ForgeApiContext;
import com.atlassian.connect.spring.ForgeApp;
import com.atlassian.connect.spring.ForgeInvocationToken;
import com.atlassian.connect.spring.ForgeSystemAccessToken;
import com.atlassian.connect.spring.ForgeSystemAccessTokenRepository;
import com.atlassian.connect.spring.internal.auth.frc.ForgeSecurityContextRetriever;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;

import java.io.IOException;
import java.net.URI;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ForgeHttpRequestInterceptorTest {

    private ForgeRemoteRequestAuthType provider;
    private ForgeRemoteRequestType remote;
    private AtlassianHostUriResolver hostUriResolver;
    private ForgeSecurityContextRetriever forgeContextRetriever;
    private ForgeSystemAccessTokenRepository forgeSystemAccessTokenRepository;

    // https://developer.atlassian.com/platform/forge/remote/essentials/#the-forge-invocation-token--fit-
    private static final String ACCESS_TOKEN = """
        eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhcmk6Y2xvdWQ6ZWNvc3lzdGVtOjphcHAvOGRiMzM4MDktMWYzMi00OGJiLThjNTItNTg3N2RhYjQ4MTA3IiwiaXNzIjoiZm9yZ2UvaW52b2NhdGlvbi10b2tlbiIsImlhdCI6MTcwMDE3NTE0OSwibmJmIjoxNzAwMTc1MTQ5LCJleHAiOjE3MDAxNzUxNzQsImp0aSI6ImQ4YTQ5NjI1M2VjOGMxOGE1NDYzMWU0YzgyY2JlZGQ1ZDBhZTg1NzAifQ.Pr6AalO7yfaKTOuvgnxxbudLyN2WnVgim5Kxh3H5Kbk
    """.trim();
    private final String forgeApiHost = "some-forge-api-host.com";
    String baseUrl = "http://" + forgeApiHost;
    private ForgeHttpRequestInterceptor sut;
    private HttpRequest request;
    private byte[] body;
    private ClientHttpRequestExecution execution;

    @BeforeEach
    void setUp() {
        provider = ForgeRemoteRequestAuthType.USER;
        remote = ForgeRemoteRequestType.CONFLUENCE;
        hostUriResolver = mock(AtlassianHostUriResolver.class);
        forgeContextRetriever = mock(ForgeSecurityContextRetriever.class);
        forgeSystemAccessTokenRepository = mock(ForgeSystemAccessTokenRepository.class);

        ForgeApp app = new ForgeApp();
        app.setApiBaseUrl(baseUrl);
        ForgeInvocationToken forgeInvocationToken = new ForgeInvocationToken();
        forgeInvocationToken.setApp(app);
        Optional<ForgeApiContext> forgeApiContext = Optional.of(new ForgeApiContext(forgeInvocationToken, Optional.of("some-user-token"), Optional.of("some-app-token")));
        when(forgeContextRetriever.getForgeApiContext()).thenReturn(forgeApiContext);
        request = mock(HttpRequest.class);
        body = new byte[0];
        execution = mock(ClientHttpRequestExecution.class);
    }

    @Test
    void shouldNotWrapRequestIfNoForgeInvocationTokenPresent() throws IOException {
        when(forgeContextRetriever.getForgeApiContext()).thenReturn(Optional.empty());
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);

        HttpRequest request = mock(HttpRequest.class);
        byte[] body = new byte[0];
        ClientHttpRequestExecution execution = mock(ClientHttpRequestExecution.class);

        sut.intercept(request, body, execution);

        verify(execution).execute(request, body);
    }

    @Test
    void shouldIncludeAppTokenUsingProvidedInstallationId() throws IOException, ParseException {
        provider = ForgeRemoteRequestAuthType.APP;
        ForgeSystemAccessToken accessToken = initForgeSystemAccessToken();
        when(forgeSystemAccessTokenRepository.findByInstallationIdAndExpirationTimeAfter(
            isA(String.class),
            isA(Timestamp.class))
        ).thenReturn(Optional.of(accessToken));
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);
        sut.setInstallationId("ari:cloud:ecosystem::installation/0a3a7799-53ae-4a5b-9e7e-03338980abb5");

        HttpRequest request = mock(HttpRequest.class);
        when(request.getURI()).thenReturn(URI.create("http://" + forgeApiHost));
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);
        byte[] body = new byte[0];
        ClientHttpRequestExecution execution = mock(ClientHttpRequestExecution.class);

        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);
        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("http://some-forge-api-host.com", wrappedRequest.getURI().toString());
        assertEquals("Bearer %s".formatted(ACCESS_TOKEN), wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0));
    }

    private static ForgeSystemAccessToken initForgeSystemAccessToken() throws ParseException {
        ForgeSystemAccessToken entity = new ForgeSystemAccessToken();
        entity.setInstallationId("ari:cloud:ecosystem::installation/0a3a7799-53ae-4a5b-9e7e-03338980abb5");
        entity.setApiBaseUrl("https://api.stg.atlassian.com/ex/confluence/d0d52620-3203-4cfa-8db5-f2587155f0dd");
        entity.setAccessToken(ACCESS_TOKEN);
        entity.parseAndSetClaims(ACCESS_TOKEN);
        return entity;
    }

    @Test
    void shouldIncludeUserTokenIfUserAuthTypePresent() throws IOException {
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);

        HttpRequest request = mock(HttpRequest.class);
        when(request.getURI()).thenReturn(URI.create("http://" + forgeApiHost));
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);

        byte[] body = new byte[0];
        ClientHttpRequestExecution execution = mock(ClientHttpRequestExecution.class);

        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);
        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("http://some-forge-api-host.com", wrappedRequest.getURI().toString());
        assertEquals("Bearer some-user-token", wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0));
    }

    @Test
    void shouldIncludeAppTokenIfAppAuthTypePresent() throws IOException {
        provider = ForgeRemoteRequestAuthType.APP;
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);

        HttpRequest request = mock(HttpRequest.class);
        when(request.getURI()).thenReturn(URI.create("http://" + forgeApiHost));
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);
        byte[] body = new byte[0];
        ClientHttpRequestExecution execution = mock(ClientHttpRequestExecution.class);

        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);
        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("http://some-forge-api-host.com", wrappedRequest.getURI().toString());
        assertEquals("Bearer some-app-token", wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0));
    }

    @Test
    void shouldNotIncludeHeadersIfNoneAuthTypePresent() throws IOException {
        provider = ForgeRemoteRequestAuthType.NONE;
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);

        HttpRequest request = mock(HttpRequest.class);
        when(request.getURI()).thenReturn(URI.create("http://" + forgeApiHost));
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);
        byte[] body = new byte[0];
        ClientHttpRequestExecution execution = mock(ClientHttpRequestExecution.class);

        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);
        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("http://some-forge-api-host.com", wrappedRequest.getURI().toString());
        assertNull(wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION));
    }

    @Test
    void shouldNotIncludeHeadersIfRequestHostAndForgeApiHostMismatch() throws IOException {
        provider = ForgeRemoteRequestAuthType.NONE;
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);

        HttpRequest request = mock(HttpRequest.class);
        when(request.getURI()).thenReturn(URI.create("http://not-" + forgeApiHost));
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);
        byte[] body = new byte[0];
        ClientHttpRequestExecution execution = mock(ClientHttpRequestExecution.class);

        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);
        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("http://not-some-forge-api-host.com", wrappedRequest.getURI().toString());
        assertNull(wrappedRequest.getHeaders().get(HttpHeaders.AUTHORIZATION));
    }

    @Test
    void shouldModifyBaseUrlCorrectlyForJiraRequest() throws IOException {
        remote = ForgeRemoteRequestType.JIRA;
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);
        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);
        URI endpoint = URI.create("http://" + forgeApiHost + "/some-endpoint");
        when(request.getURI()).thenReturn(endpoint);
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);

        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("http://some-forge-api-host.com/some-endpoint", wrappedRequest.getURI().toString());
    }

    @Test
    void shouldModifyBaseUrlCorrectlyForConfluenceRequest() throws IOException {
        remote = ForgeRemoteRequestType.CONFLUENCE;
        URI endpoint = URI.create("/some-endpoint");
        when(request.getURI()).thenReturn(endpoint);
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);
        when(hostUriResolver.resolveToAbsoluteUriWithBase(any(), any())).thenReturn(URI.create(baseUrl + "/wiki/some-endpoint"));
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);
        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);

        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("http://some-forge-api-host.com/wiki/some-endpoint", wrappedRequest.getURI().toString());
    }

    @Test
    void shouldModifyBaseUrlCorrectlyForOtherRequest() throws IOException {
        remote = ForgeRemoteRequestType.OTHER;
        URI endpoint = URI.create("/some-endpoint");
        when(request.getURI()).thenReturn(endpoint);
        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);
        when(hostUriResolver.resolveToAbsoluteUriWithBase(any(), any())).thenReturn(URI.create("https://" + forgeApiHost));
        sut = new ForgeHttpRequestInterceptor(
            provider,
            remote,
            hostUriResolver,
            forgeContextRetriever,
            forgeSystemAccessTokenRepository,
            forgeApiHost);
        ArgumentCaptor<HttpRequest> requestCaptor = ArgumentCaptor.forClass(HttpRequest.class);

        sut.intercept(request, body, execution);

        verify(execution).execute(requestCaptor.capture(), eq(body));
        HttpRequest wrappedRequest = requestCaptor.getValue();

        assertEquals("https://some-forge-api-host.com", wrappedRequest.getURI().toString());
    }

}
