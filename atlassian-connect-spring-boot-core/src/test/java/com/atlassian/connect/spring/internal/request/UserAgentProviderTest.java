package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.system.JavaVersion;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserAgentProviderTest {

    @Mock
    private AddonDescriptor addonDescriptor;

    private final UserAgentProvider userAgentProvider = new UserAgentProvider(null, null);

    @Test
    void shouldReturnUserAgentWithoutVersions() {
        assertThat(userAgentProvider.getUserAgent("1.0.0", null, null, null, addonDescriptor), is("atlassian-connect-spring-boot/1.0.0"));
    }

    @Test
    void shouldReturnUserAgentWithVersions() {
        when(addonDescriptor.getKey()).thenReturn("fake-key");

        assertThat(userAgentProvider.getUserAgent("1.0.0", "3.1.1", "6.0.9", JavaVersion.SEVENTEEN, addonDescriptor),
                is("atlassian-connect-spring-boot/1.0.0 SpringBoot/3.1.1 Spring/6.0.9 Java/17 fake-key"));
    }
}
