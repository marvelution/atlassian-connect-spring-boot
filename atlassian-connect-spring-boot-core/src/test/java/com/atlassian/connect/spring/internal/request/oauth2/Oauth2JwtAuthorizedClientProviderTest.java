package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.security.oauth2.core.OAuth2AccessToken;

import java.time.Clock;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class Oauth2JwtAuthorizedClientProviderTest {

    private static final long REFERENCE_NOW = 1500; // arbitrary definition of now value

    @Mock
    private Clock clock;

    @Mock
    private OAuth2JwtAccessTokenResponseClient client;

    @Mock
    private AtlassianConnectSecurityContextHelper securityContextHelper;

    @InjectMocks
    private OAuth2JwtAuthorizedClientProvider oAuth2JwtAuthorizedClientProvider;

    @BeforeEach
    void setUp() {
        when(clock.instant()).thenReturn(Instant.ofEpochSecond(REFERENCE_NOW));

        oAuth2JwtAuthorizedClientProvider.setClock(clock);
    }

    @Test
    void testExpiry() {
        Instant issuedAt = Instant.ofEpochSecond(REFERENCE_NOW - 900);
        Instant expiry = Instant.ofEpochSecond(REFERENCE_NOW);

        AbstractOAuth2Token token
                = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, "rhubarb", issuedAt, expiry);
        boolean willExpire = oAuth2JwtAuthorizedClientProvider.willTokenExpire(token);

        assertTrue(willExpire, "Token should be expected to expire if used at the expiry time");
    }

    @Test
    void testExpiryAtSkew() {
        Instant issuedAt = Instant.ofEpochSecond(REFERENCE_NOW - 900);
        Instant expiry = Instant.ofEpochSecond(REFERENCE_NOW + 59);

        AbstractOAuth2Token token
                = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, "rhubarb", issuedAt, expiry);
        boolean willExpire = oAuth2JwtAuthorizedClientProvider.willTokenExpire(token);

        assertTrue(willExpire, "Token is close to expiry so should be refreshed");
    }

    @Test
    void testUnexpired() {
        Instant issuedAt = Instant.ofEpochSecond(REFERENCE_NOW - 900);
        Instant expiry = Instant.ofEpochSecond(REFERENCE_NOW + 60);

        AbstractOAuth2Token token
                = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, "rhubarb", issuedAt, expiry);
        boolean willExpire = oAuth2JwtAuthorizedClientProvider.willTokenExpire(token);

        assertFalse(willExpire, "Token is within skew period and is ok to continue using");
    }
}
