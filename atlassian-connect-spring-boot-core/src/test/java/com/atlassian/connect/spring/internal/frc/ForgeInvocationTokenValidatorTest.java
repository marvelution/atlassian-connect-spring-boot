package com.atlassian.connect.spring.internal.frc;

import com.atlassian.connect.spring.ForgeInvocationToken;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.auth.frc.ForgeInvocationTokenValidator;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;
import org.springframework.web.server.ResponseStatusException;

import java.net.URL;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ForgeInvocationTokenValidatorTest {
    private ForgeInvocationTokenValidator sut;

    private String expectedAppId;
    private RSAKey rsaJWK;
    private RSAKey rsaPublicJWK;
    private AtlassianConnectProperties mockAtlassianConnectProperties;
    private Environment mockEnv;

    @BeforeEach
    public void setup() throws JOSEException {
        rsaJWK = new RSAKeyGenerator(2048)
                .keyUse(KeyUse.SIGNATURE)
                .keyID("123")
                .generate();
        rsaPublicJWK = rsaJWK.toPublicJWK();
        expectedAppId = "placeholder";
        mockEnv = mock(Environment.class);
        mockAtlassianConnectProperties = mock(AtlassianConnectProperties.class);
    }

    @Test
    void testGetBearerToken() {
        sut = new ForgeInvocationTokenValidator(mockAtlassianConnectProperties, mockEnv);
        String validAuthorizationHeader = "Bearer validToken";
        String result = sut.getBearerToken(validAuthorizationHeader);
        assertNotNull(result);
        assertEquals("validToken", result);
    }

    @Test
    void testGetBearerTokenWithInvalidAuthHeader() {
        String invalidAuthorizationHeader = "invalidToken";
        sut = new ForgeInvocationTokenValidator(mockAtlassianConnectProperties, mockEnv);
        assertThrows(ResponseStatusException.class, () -> sut.getBearerToken(invalidAuthorizationHeader));
    }

    @Test
    void testGetBearerTokenWithNullAuthHeader() {
        String nullAuthorizationHeader = null;
        sut = new ForgeInvocationTokenValidator(mockAtlassianConnectProperties, mockEnv);
        assertThrows(ResponseStatusException.class, () -> sut.getBearerToken(nullAuthorizationHeader));
    }

    @Test
    void testValidate() throws JOSEException {
        try (MockedStatic<JWKSet> mockJwkSet = Mockito.mockStatic(JWKSet.class)) {
            when(mockAtlassianConnectProperties.getForgeInvocationTokenJwksUrl()).thenReturn("http://test:8080/jwks");
            mockJwkSet.when(() -> JWKSet.load((URL) any())).thenReturn(new JWKSet(rsaPublicJWK));

            sut = new ForgeInvocationTokenValidator(mockAtlassianConnectProperties, mockEnv);

            String validJwt = generateToken();
            String validAuthorizationHeader = "Bearer " + validJwt;
            String invalidAuthorizationHeader = "invalidToken";
            String nullAuthorizationHeader = null;
            String authorizationHeaderWithNoSpaces = "Bearer" + validJwt;

            // Test with valid authorization header
            JWTClaimsSet result = sut.validate(validAuthorizationHeader, expectedAppId);
            assertNotNull(result);

            String validAuthorizationHeaderLowerCase = "bearer " + validJwt;
            result = sut.validate(validAuthorizationHeaderLowerCase, expectedAppId);
            assertNotNull(result);

            // Test with invalid authorization header
            assertThrows(ResponseStatusException.class, () -> sut.validate(invalidAuthorizationHeader, expectedAppId));

            // Test with null authorization header
            assertThrows(ResponseStatusException.class, () -> sut.validate(nullAuthorizationHeader, expectedAppId));

            //Test with bearer token with no spaces
            assertThrows(ResponseStatusException.class, () -> sut.validate(authorizationHeaderWithNoSpaces, expectedAppId));
        }
    }

    @Test
    void testDecode() {
        try (MockedStatic<JWKSet> mockJwkSet = Mockito.mockStatic(JWKSet.class)) {
            mockJwkSet.when(() -> JWKSet.load((URL) any())).thenReturn(new JWKSet(rsaPublicJWK));

            sut = new ForgeInvocationTokenValidator(mockAtlassianConnectProperties, mockEnv);

            // Test with valid JWT claims
            JWTClaimsSet validJwtClaimsSet = new JWTClaimsSet.Builder()
                    .issuer("forge/invocation-token")
                    .audience(expectedAppId)
                    .subject("forge/invocation-token")
                    .expirationTime(new Date(new Date().getTime() + 60 * 1000)).build();
            ForgeInvocationToken result = sut.decode(validJwtClaimsSet);
            assertNotNull(result);

            // Create null JWT claims
            JWTClaimsSet nullJwtClaimsSet = null;

            // Test with null JWT claims
            assertThrows(NullPointerException.class, () -> sut.decode(nullJwtClaimsSet));
        }
    }

    private String generateToken() throws JOSEException {
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer("forge/invocation-token")
                .audience(expectedAppId)
                .subject("forge/invocation-token")
                .expirationTime(new Date(new Date().getTime() + 60 * 1000))
                .build();

        JWSSigner signer = new RSASSASigner(rsaJWK);
        SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(rsaJWK.getKeyID()).build(), claimsSet);
        signedJWT.sign(signer);

        return signedJWT.serialize();
    }
}
