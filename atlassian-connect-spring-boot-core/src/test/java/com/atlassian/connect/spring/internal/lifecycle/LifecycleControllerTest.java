package com.atlassian.connect.spring.internal.lifecycle;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class LifecycleControllerTest {

    @Test
    void shouldReturnInstalledMethodReference() {
        assertThat(LifecycleController.getInstalledMethod().getName(), is("installed"));
    }

    @Test
    void shouldReturnUninstalledMethodReference() {
        assertThat(LifecycleController.getUninstalledMethod().getName(), is("uninstalled"));
    }
}
