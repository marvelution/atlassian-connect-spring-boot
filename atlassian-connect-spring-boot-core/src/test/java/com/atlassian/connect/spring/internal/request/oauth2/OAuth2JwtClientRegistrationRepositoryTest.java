package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.oauth2.client.registration.ClientRegistration;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class OAuth2JwtClientRegistrationRepositoryTest {
    private static final String TEST_REG_ID = "test-registration-id";
    private static final String TENANT_URL_IN_STG = "https://test.jira-dev.com";
    private static final String TENANT_URL_IN_PROD = "https://test.atlassian.net";

    private static final String TENANT_URL_IN_FEDRAMP_STG = "https://test.jira-dev.com";
    private static final String TENANT_URL_IN_FEDRAMP_PROD = "https://test.atlassian.net";
    private static final String AUTHORIZATION_SERVER_URL_STAGING = "https://oauth-2-authorization-server.stg.services.atlassian.com";
    private static final String AUTHORIZATION_SERVER_URL_FEDRAMP_STAGING = "https://oauth-2-authorization-server.stg.services.atlassian.com";

    private static final String AUTHORIZATION_SERVER_URL_PRODUCTION = "https://oauth-2-authorization-server.services.atlassian.com";
    private static final String AUTHORIZATION_SERVER_URL_FEDRAMP_PRODUCTION = "https://oauth-2-authorization-server.services.atlassian.com";

    private static final String TOKEN_POSTFIX = "/oauth2/token";

    private final AtlassianHost host = new AtlassianHost();

    @Mock
    private AtlassianHostRepository atlassianHostRepository;

    @InjectMocks
    private OAuth2JwtClientRegistrationRepository oAuth2JwtClientRegistrationRepository;

    @BeforeEach
    void setUp() {
        host.setClientKey("test-client-key");
    }

    @Test
    void shouldUseProdOAuth2ServerUrlForProdTenant() {
        host.setBaseUrl(TENANT_URL_IN_PROD);
        doReturn(Optional.of(host)).when(atlassianHostRepository).findById(TEST_REG_ID);
        ClientRegistration cr = oAuth2JwtClientRegistrationRepository.findByRegistrationId(TEST_REG_ID);
        assertThat(cr.getProviderDetails().getTokenUri(), is(AUTHORIZATION_SERVER_URL_PRODUCTION + TOKEN_POSTFIX));
    }

    @Test
    void shouldUseFedRAMPProdOAuth2ServerUrlForProdFedRAMPTenant() {
        host.setBaseUrl(TENANT_URL_IN_FEDRAMP_PROD);
        doReturn(Optional.of(host)).when(atlassianHostRepository).findById(TEST_REG_ID);
        ClientRegistration cr = oAuth2JwtClientRegistrationRepository.findByRegistrationId(TEST_REG_ID);
        assertThat(cr.getProviderDetails().getTokenUri(), is(AUTHORIZATION_SERVER_URL_FEDRAMP_PRODUCTION + TOKEN_POSTFIX));
    }

    @Test
    void shouldUseStgOAuth2ServerUrlForStgTenant() {
        host.setBaseUrl(TENANT_URL_IN_STG);
        doReturn(Optional.of(host)).when(atlassianHostRepository).findById(TEST_REG_ID);
        ClientRegistration cr = oAuth2JwtClientRegistrationRepository.findByRegistrationId(TEST_REG_ID);
        assertThat(cr.getProviderDetails().getTokenUri(), is(AUTHORIZATION_SERVER_URL_STAGING + TOKEN_POSTFIX));
    }

    @Test
    void shouldUseFedRAMPStgOAuth2ServerUrlForFedRAMPStgTenant() {
        host.setBaseUrl(TENANT_URL_IN_FEDRAMP_STG);
        doReturn(Optional.of(host)).when(atlassianHostRepository).findById(TEST_REG_ID);
        ClientRegistration cr = oAuth2JwtClientRegistrationRepository.findByRegistrationId(TEST_REG_ID);
        assertThat(cr.getProviderDetails().getTokenUri(), is(AUTHORIZATION_SERVER_URL_FEDRAMP_STAGING + TOKEN_POSTFIX));
    }

}
