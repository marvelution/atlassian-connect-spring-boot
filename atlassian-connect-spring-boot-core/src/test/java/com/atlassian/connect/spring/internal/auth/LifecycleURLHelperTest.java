package com.atlassian.connect.spring.internal.auth;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LifecycleURLHelperTest {
    @Mock
    AddonDescriptorLoader addonDescriptorLoader;

    @Mock
    HttpServletRequest request;

    @Mock
    AddonDescriptor addonDescriptor;

    @InjectMocks
    LifecycleURLHelper lifecycleURLHelper;

    @BeforeEach
    public void init() {
        when(addonDescriptor.getBaseUrl()).thenReturn("https://example.com/");
        when(addonDescriptorLoader.getDescriptor()).thenReturn(addonDescriptor);
    }

    @Test
    void shouldReturnTrueWithMatchingInstallUrlPath() {
        when(addonDescriptor.getInstalledLifecycleUrl()).thenReturn("/installed");
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/installed"));
        assertThat(lifecycleURLHelper.isRequestToInstalledLifecycle(request), is(true));
    }

    @Test
    void shouldReturnFalseWithNotMatchingInstallUrlPath() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/different_path"));
        assertThat(lifecycleURLHelper.isRequestToInstalledLifecycle(request), is(false));
    }

    @Test
    void shouldReturnTrueWithMatchingUninstallUrlPath() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/uninstalled"));
        assertThat(lifecycleURLHelper.isRequestToUninstalledLifecycle(request), is(false));
    }

    @Test
    void shouldReturnFalseWithNotMatchingUninstallUrlPath() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/different_path"));
        assertThat(lifecycleURLHelper.isRequestToUninstalledLifecycle(request), is(false));
    }

    @Test
    void shouldReturnFalseIfInstallUrlIsNotDefined() {
        when(addonDescriptor.getInstalledLifecycleUrl()).thenReturn(null);
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/installed"));
        assertThat(lifecycleURLHelper.isRequestToInstalledLifecycle(request), is(false));
    }

    @Test
    void shouldReturnFalseIfUninstallUrlIsNotDefined() {
        when(addonDescriptor.getUninstalledLifecycleUrl()).thenReturn(null);
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/uninstalled"));
        assertThat(lifecycleURLHelper.isRequestToUninstalledLifecycle(request), is(false));

    }

    @Test
    void isRequestToLifecycleURLShouldReturnTrueWhenInstallLifecycleUrlMatches() {
        when(addonDescriptor.getInstalledLifecycleUrl()).thenReturn("/installed");
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/installed"));
        assertThat(lifecycleURLHelper.isRequestToLifecycleURL(request), is(true));
    }

    @Test
    void isRequestToLifecycleURLShouldReturnTrueWhenUninstallLifecycleUrlMatches() {
        when(addonDescriptor.getUninstalledLifecycleUrl()).thenReturn("/uninstalled");
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/uninstalled"));
        assertThat(lifecycleURLHelper.isRequestToLifecycleURL(request), is(true));
    }

    @Test
    void isRequestToLifecycleURLShouldReturnFalseWhenUrlIsNotALifecycleOne() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("https://example.com/normal"));
        assertThat(lifecycleURLHelper.isRequestToLifecycleURL(request), is(false));
    }

    @Test
    void isRequestToLifecycleURLShouldReturnFalseWhenUrlIsEmpty() {
        when(request.getRequestURL()).thenReturn(new StringBuffer());
        assertThat(lifecycleURLHelper.isRequestToLifecycleURL(request), is(false));
    }
}
