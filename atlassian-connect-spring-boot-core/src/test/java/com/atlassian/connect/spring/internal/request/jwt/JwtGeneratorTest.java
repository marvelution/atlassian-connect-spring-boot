package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;

import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class JwtGeneratorTest {

    @InjectMocks
    private JwtGenerator jwtGenerator;

    @Test
    void shouldRejectRelativeUriWithoutHost() {
        URI uri = URI.create("/api");

        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> jwtGenerator.createJwtToken(HttpMethod.GET, uri));

        assertThat(exception.getMessage(), is("The given URI is not absolute"));
    }

    @Test
    void shouldRejectRelativeUriWithHost() {
        URI uri = URI.create("/api");
        AtlassianHost host = new AtlassianHost();

        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> jwtGenerator.createJwtToken(HttpMethod.GET, uri, host));

        assertThat(exception.getMessage(), is("The given URI is not absolute"));
    }

    @Test
    void shouldFailOnHostUriMismatch() {
        URI uri = URI.create("http://example.com/api");
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl("http://foo.atlassian.net/");
        
        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> jwtGenerator.createJwtToken(HttpMethod.GET, uri, host));

        assertThat(exception.getMessage(), is("The given URI is not under the base URL of the given host"));
    }
}
