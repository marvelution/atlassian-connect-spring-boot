package com.atlassian.connect.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation used on <a href="https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc">Spring Web MVC</a>
 * {@link org.springframework.stereotype.Controller}s or individual controller methods to denote that authentication by
 * JSON Web Token is not required. This should generally only be used for endpoints that will not be accessed by an
 * Atlassian host.
 *
 * <p>To accept requests without requiring JWT authentication, add this annotation to the class
 * or method:
 * <blockquote><pre><code> &#64;Controller
 * &#64;IgnoreJwt
 * public class PublicController {
 *     ...
 * }</code></pre></blockquote>
 *
 * @since 1.0.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreJwt {}
