package com.atlassian.connect.spring;

import java.util.Optional;

public class ForgeApiContext {
    private ForgeInvocationToken forgeInvocationToken;

    private Optional<String> userToken;

    private Optional<String> appToken;

    public ForgeApiContext(ForgeInvocationToken forgeInvocationToken, Optional<String> userToken, Optional<String> appToken) {
        this.forgeInvocationToken = forgeInvocationToken;
        this.userToken = userToken;
        this.appToken = appToken;
    }

    public ForgeInvocationToken getForgeInvocationToken() {
        return forgeInvocationToken;
    }

    public void setForgeInvocationToken(ForgeInvocationToken forgeInvocationToken) {
        this.forgeInvocationToken = forgeInvocationToken;
    }

    public Optional<String> getUserToken() {
        return userToken;
    }

    public void setUserToken(Optional<String> userToken) {
        this.userToken = userToken;
    }

    public Optional<String> getAppToken() {
        return appToken;
    }

    public void setAppToken(Optional<String> appToken) {
        this.appToken = appToken;
    }
}
