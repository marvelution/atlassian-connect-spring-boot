package com.atlassian.connect.spring;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * POJO Representation of the Forge Invocation Token Claims
 */
public class ForgeInvocationToken {

    /**
     * Information about the Forge app that is making the request
     */
    private ForgeApp app;

    /**
     * Invocation Context
     */
    private JsonNode context;

    /**
     * The audience that the token was intended for. This should be your appId and must be validated.
     */
    private String aud;

    /**
     * The identifier for the user who invoked the app. UI modules only.
     */
    private String principal;

    /**
     * The issuer of the token. This should be forge/invocation-token and must be validated.
     */
    private String iss;

    public ForgeInvocationToken() {
    }

    public ForgeApp getApp() {
        return app;
    }

    public void setApp(ForgeApp app) {
        this.app = app;
    }

    public JsonNode getContext() {
        return context;
    }

    public void setContext(JsonNode context) {
        this.context = context;
    }

    public String getAud() {
        return aud;
    }

    public void setAud(String aud) {
        this.aud = aud;
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }
}
