package com.atlassian.connect.spring;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.annotation.Id;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.Instant;
import java.util.Objects;

/**
 * System access token. To know more details,
 * see <a href="https://developer.atlassian.com/platform/forge/remote/essentials/">Forge remote essentials</a>.
 *
 * @see ForgeSystemAccessTokenRepository
 */
public class ForgeSystemAccessToken {

    private static final Logger log = LoggerFactory.getLogger(ForgeSystemAccessToken.class);

    /**
     * The string (in ARI format, e.g. ari:cloud:ecosystem::installation/c3658f0f-8380-41e5-bb1e-68903f8efdca)
     * identifying the installation of the app on Forge. Stable across upgrades but not uninstallation and
     * re-installation. This value should be used to key Forge-related tenant details in your add-on.
     */
    @Id
    private String installationId; // "ari:cloud:ecosystem::installation/00000000-0000-0000-0000-000000000000"

    private String apiBaseUrl;  // "https://api.atlassian.com/ex/confluence/00000000-0000-0000-0000-000000000000"
    private String accessToken;
    private Timestamp expirationTime;

    public ForgeSystemAccessToken() {
    }

    public String getInstallationId() {
        return installationId;
    }

    public String getApiBaseUrl() {
        return apiBaseUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public Timestamp getExpirationTime() {
        return expirationTime;
    }

    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setExpirationTime(Timestamp expirationTime) {
        this.expirationTime = expirationTime;
    }

    public void parseAndSetClaims(String appToken) throws ParseException {
        JWTClaimsSet claimsSet = JWTParser.parse(appToken).getJWTClaimsSet();
        this.setExpirationTime(Timestamp.from(claimsSet.getExpirationTime().toInstant()));
    }

    public static boolean isFulfilled(ForgeSystemAccessToken entity) {
        return Objects.nonNull(entity.getInstallationId())
            && Objects.nonNull(entity.getApiBaseUrl())
            && Objects.nonNull(entity.getAccessToken())
            && Objects.nonNull(entity.getExpirationTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ForgeSystemAccessToken that)) return false;
        return Objects.equals(installationId, that.installationId)
            && Objects.equals(apiBaseUrl, that.apiBaseUrl)
            && Objects.equals(accessToken, that.accessToken)
            && Objects.equals(expirationTime, that.expirationTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(installationId, apiBaseUrl, accessToken, expirationTime);
    }
}
