package com.atlassian.connect.spring;

import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.sql.Timestamp;
import java.util.Optional;

/**
 * A <a href="https://spring.io/projects/spring-data/">Spring Data</a> repository for
 * storing access tokens parsed from Forge Invocation Token (FIT).
 *
 * <p>For guidance on configuring persistence for your application, see {@link AtlassianHostRepository}</p>
 *
 * @since 5.2.0
 */
public interface ForgeSystemAccessTokenRepository extends CrudRepository<ForgeSystemAccessToken, String> {

    /**
     * Find the access token that is not yet expired
     *
     * @param installationId the installation ID
     * @param expirationTime In most cases, specify the timestamp as of the moment this method is called with a bit of leeway
     * @return unexpired access token and its meta data
     */
    @NonNull
    Optional<ForgeSystemAccessToken> findByInstallationIdAndExpirationTimeAfter(
        @NonNull String installationId,
        @NonNull Timestamp expirationTime);

    /**
     * Clean up the stale records
     *
     * @param expirationTime In most cases, specify the timestamp as of the moment this method is called
     * @return the number of affected rows
     */
    long deleteAllByExpirationTimeBefore(@NonNull Timestamp expirationTime);
}
