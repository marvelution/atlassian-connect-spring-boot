package com.atlassian.connect.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation used on <a href="https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc">Spring Web MVC</a>
 * {@link org.springframework.stereotype.Controller}s or individual controller methods to verify that the request is
 * coming from an Atlassian Forge app to this remote backend endpoint as per the
 * <a href="https://developer.atlassian.com/platform/forge/forge-remote-overview/#remote-contract">Forge Remote contract</a>.
 * <p>To accept requests coming from a Forge app, add this annotation to the method:
 * <blockquote><pre><code> &#64;Controller
 * &#64;ForgeRemote
 * public class PublicController {
 *     ...
 * }
 *
 * To access Connect context variables via `AtlassianHostUser`, use the following annotation in the method:
 *  &#64;ForgeRemote(associateConnect=true)
 *  public class PublicController {
 *       ...
 *  }
 * </code></pre></blockquote>
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ForgeRemote {
    boolean associateConnect() default false;
}
