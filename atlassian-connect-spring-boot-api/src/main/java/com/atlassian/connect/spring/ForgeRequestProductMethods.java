package com.atlassian.connect.spring;

import org.springframework.web.client.RestTemplate;

/**
 * A {@link ForgeRequestProductMethods} that provides methods to make Forge authenticated requests to the Atlassian products
 */
public interface ForgeRequestProductMethods {

    /**
     * For making requests to the Atlassian API using Forge authentication.
     * You can call either {@link com.atlassian.connect.spring.AtlassianForgeRestClients#asUser()} or {@link AtlassianForgeRestClients#asApp()} or {@link AtlassianForgeRestClients#asApp(String)} and then chain it with the product you are targeting
     * @return a REST client for making requests to the Atlassian API using Forge authentication
     */
    RestTemplate request();

    /**
     * For making requests to Confluence using Forge authentication.
     * You can call either {@link com.atlassian.connect.spring.AtlassianForgeRestClients#asUser()} or {@link AtlassianForgeRestClients#asApp()} or {@link AtlassianForgeRestClients#asApp(String)} and then chain it with the product you are targeting
     * @return a REST client for making requests to Confluence using Forge authentication
     */
    RestTemplate requestConfluence();

    /**
     * For making requests to Jira using Forge authentication.
     * You can call either {@link com.atlassian.connect.spring.AtlassianForgeRestClients#asUser()} or {@link AtlassianForgeRestClients#asApp()} or {@link AtlassianForgeRestClients#asApp(String)} and then chain it with the product you are targeting
     * @return a REST client for making requests to Jira using Forge authentication
     */
    RestTemplate requestJira();

}
