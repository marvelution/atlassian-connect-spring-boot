package com.atlassian.connect.spring;

import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;

/**
 * A <a href="https://spring.io/projects/spring-data/">Spring Data</a> repository for
 * associating the Forge installation identifier with the corresponding Connect addon data
 *
 * <p>For guidance on configuring persistence for your application, see {@link AtlassianHostRepository}</p>
 *
 * @since 4.1.0
 */
public interface AtlassianHostMappingRepository extends CrudRepository<AtlassianHostMapping, String> {

    /**
     * Delete all the mappings that are associated with a specific clientKey
     *
     * @param clientKey the associated client key
     * @return the amount of mappings that were deleted
     */
    long deleteByClientKey(String clientKey);

    /**
     * Find the AtlassianHostMapping that is associated with a specific installationId
     *
     * @param installationId the associated installationId
     * @return the clientKey that is associated with the installationId
     */
    @Override
    @NonNull
    Optional<AtlassianHostMapping> findById(@NonNull String installationId);
}
