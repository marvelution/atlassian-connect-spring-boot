package com.atlassian.connect.spring;

/**
 * A {@link ForgeRequestProductMethods} that provides methods to make Forge authenticated requests to the Atlassian products
 */
public abstract class AtlassianForgeRestClients implements ForgeRequestProductMethods {

    /**
     * For making requests to Atlassian products using OAuth 2.0
     * The principal of the request is the app.
     *
     * @return a REST client
     */
    public abstract ForgeRequestProductMethods asApp();

    /**
     * For making requests to Atlassian products using a cached OAuth 2.0 access token.
     * This method is mainly for the Forge remote that sends a request proactively outside the `@ForgeRemote` context.
     * The principal of the request is the app.
     *
     * @return a REST client
     */
    public abstract ForgeRequestProductMethods asApp(String installationId);

    /**
     * For making requests to Atlassian products using OAuth 2.0
     * The principal of the request is the user.
     *
     * @return a REST client
     */
    public abstract ForgeRequestProductMethods asUser();

}
