package com.atlassian.connect.spring;

import org.springframework.context.ApplicationEvent;

/**
 * A <a href="https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#context-functionality-events">Spring application event</a>
 * fired when the add-on has been successfully installed on a host.
 *
 * <p>To listen to this event, use the {@link org.springframework.context.event.EventListener} annotation:
 * <blockquote><pre><code> &#64;EventListener
 * public void addonInstalled(AddonInstalledEvent event) {
 *     ...
 * }</code></pre></blockquote>
 *
 * <p><strong>NOTE:</strong> This event is fired asynchronously and cannot affect the HTTP response returned to the
 * Atlassian host.
 *
 * @since 1.0.0
 */
public class AddonInstalledEvent extends ApplicationEvent {

    /**
     * The host for which this event occurred.
     */
    private final AtlassianHost host;

    /**
     * Creates a new event.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     * @param host   the host for which the event occurred
     */
    public AddonInstalledEvent(Object source, AtlassianHost host) {
        super(source);
        this.host = host;
    }

    /**
     * Returns the host for which the event occurred.
     *
     * @return the Atlassian host
     */
    public AtlassianHost getHost() {
        return host;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("AddonInstalledEvent{host=%s}", host);
    }
}
