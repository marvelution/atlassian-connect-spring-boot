package com.atlassian.connect.spring;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddonAuthenticationTypeTest {

    @ParameterizedTest
    @MethodSource("provideParameters")
    void shouldHaveCorrectOrdinalValues(int expectedOrdinalValue, AddonAuthenticationType enumValue) {
        assertEquals(expectedOrdinalValue, enumValue.ordinal(), "The enum ordinal values cannot be changed since they are stored in the db");
    }

    private static Stream<Arguments> provideParameters() {
        return Stream.of(
                Arguments.of(0, AddonAuthenticationType.JWT)
        );
    }
}
